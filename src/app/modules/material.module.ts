import {NgModule} from '@angular/core';
import {
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule
} from '@angular/material';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {SatDatepickerModule, SatNativeDateModule} from 'saturn-datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
    imports: [
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatInputModule,
        MatAutocompleteModule,
        DragDropModule,
        ScrollingModule,
        MatDialogModule,
        MatTooltipModule,
        MatSnackBarModule,
        SatDatepickerModule,
        SatNativeDateModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule,
        MatPaginatorModule
    ],
    exports: [
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatInputModule,
        MatAutocompleteModule,
        DragDropModule,
        ScrollingModule,
        MatDialogModule,
        MatTooltipModule,
        MatSnackBarModule,
        SatDatepickerModule,
        SatNativeDateModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule,
        MatPaginatorModule
    ],
    providers: [
        MatDatepickerModule,
        {provide: MatDialogRef, useValue: {}},
        {provide: MAT_DIALOG_DATA, useValue: []},
    ],
})

export class MaterialModule {
}
