import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { SharedModule } from './shared.module';
import { ProfileComponent } from '../components/user/profile/profile.component';
import { ProfilePageComponent } from '../components/user/profile/profile-page/profile-page.component';
import { HistoryPageComponent } from '../components/user/profile/history-page/history-page.component';
import { AvatarControlsDirective } from '../directives/avatar-controls.directive';
import { HistoryListItemComponent } from '../components/user/profile/history-page/history-list-item/history-list-item.component';
import { HistoryItemDirective } from '../directives/history-item.directive';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { ProfileFormComponent } from '../components/user/profile/profile-page/profile-form/profile-form.component';
import { ProfileAvatarComponent } from '../components/user/profile/profile-page/profile-avatar/profile-avatar.component';
import { MaterialModule } from './material.module';
import { PreviewTripComponent } from '../components/user/profile/history-page/preview-trip/preview-trip.component';
import { ProfileSocialsComponent } from '../components/user/profile/profile-page/profile-socials/profile-socials.component';

@NgModule({
  declarations: [
    ProfileComponent,
    ProfilePageComponent,
    ProfileFormComponent,
    ProfileAvatarComponent,
    HistoryPageComponent,
    AvatarControlsDirective,
    HistoryListItemComponent,
    HistoryItemDirective,
    PreviewTripComponent,
    ProfileSocialsComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    GooglePlaceModule,
    SharedModule,
    MaterialModule
  ],
  providers: [
  ],
  entryComponents: [
    PreviewTripComponent
  ]
})
export class UserModule { }
