import { NgModule } from '@angular/core';
import { environment } from '../../environments/environment';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';
import { SharedModule } from './shared.module';
import { ResizableModule } from 'angular-resizable-element';
import { RecommendationComponent } from '../components/public/recommendation/recommendation.component';
import { RecommendationRowComponent } from '../components/public/recommendation/recommendation-row/recommendation-row.component';
import { CityComponent } from '../components/public/city/city.component';
import { CityRowComponent } from '../components/public/city/city-row/city-row.component';
import { GeoLocationMapComponent } from '../components/public/geo-location-map/geo-location-map.component';
import { InputSearchByTextComponent } from '../components/public/shared/input-search-by-text/input-search-by-text.component';
import { SelectSortByComponent } from '../components/public/shared/select-sort-by/select-sort-by.component';
import { BtnSynchronizeGoogleCalendarComponent } from '../components/public/shared/btn-synchronize-google-calendar/btn-synchronize-google-calendar.component';
import { SwiperOfCityCardsComponent } from '../components/public/swiper-of-city-cards/swiper-of-city-cards.component';
import { SWIPER_CONFIG, SwiperConfigInterface, SwiperModule } from 'ngx-swiper-wrapper';
import { StarsRaitingComponent } from '../components/public/stars-rating/stars-raiting.component';
import { EventComponent } from '../components/public/event/event.component';
import { BookingDetailsComponent } from '../components/public/booking-details/booking-details.component';
import { PlanTableComponent } from '../components/public/booking-details/plan-table/plan-table.component';
import { FlightDataComponent } from '../components/public/booking-details/flight-data/flight-data.component';
import { EventTypesComponent } from '../components/public/booking-details/event-types/event-types.component';
import { EventsModalComponent } from '../components/public/booking-details/events-modal/events-modal.component';
import { TripDetailsComponent } from '../components/public/trip-details/trip-details.component';
import { ReviewItemComponent } from '../components/public/trip-details/review-item/review-item.component';
import { ReviewControlsComponent } from '../components/public/trip-details/review-controls/review-controls.component';
import { ImmutableRatingComponent } from '../components/public/stars-rating/immutable-rating/immutable-rating.component';
import { CommentItemComponent } from '../components/public/trip-details/comment-item/comment-item.component';
import { ReviewFormComponent } from '../components/public/trip-details/review-form/review-form.component';
import { AgmCoreModule } from '@agm/core';
import { RecommendationCardComponent } from '../components/public/recommendation/recommendation-row/recommendation-card/recommendation-card.component';


const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  centeredSlides: false,
};


@NgModule({
  declarations: [
    RecommendationComponent,
    RecommendationRowComponent,
    CityComponent,
    CityRowComponent,
    GeoLocationMapComponent,
    InputSearchByTextComponent,
    BtnSynchronizeGoogleCalendarComponent,
    SelectSortByComponent,
    SwiperOfCityCardsComponent,
    StarsRaitingComponent,
    EventComponent,
    BookingDetailsComponent,
    PlanTableComponent,
    FlightDataComponent,
    EventTypesComponent,
    EventsModalComponent,
    TripDetailsComponent,
    ReviewItemComponent,
    ReviewControlsComponent,
    ReviewFormComponent,
    ImmutableRatingComponent,
    CommentItemComponent,
    RecommendationCardComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    SwiperModule,
    ResizableModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleKey
    }),
  ],
  exports: [
    RecommendationComponent,
    CityComponent
  ],
  entryComponents: [
    EventsModalComponent
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class BookingModule { }
