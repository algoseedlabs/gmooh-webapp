import { NgModule } from '@angular/core';
import { environment } from '../../environments/environment';
import { CommonModule } from '@angular/common';
import { ForgotPasswordComponent } from '../components/public/auth/forgot-password/forgot-password.component';
import { LoginComponent } from '../components/public/auth/login/login.component';
import { RegistrationComponent } from '../components/public/auth/registration/registration.component';
import { EmailComponent } from '../components/public/auth/registration/email/email.component';
import { InformationComponent } from '../components/public/auth/registration/information/information.component';
import { AvatarComponent } from '../components/public/auth/registration/avatar/avatar.component';
import { SocialButtonsComponent } from '../components/public/auth/social-buttons/social-buttons.component';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider, LoginOpt } from 'angularx-social-login';
import { FlyingPlaneDirective } from '../directives/flying-plane.directive';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { MaterialModule } from './material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileMoreInfoComponent } from '../components/public/@dialogs/profile-more-info/profile-more-info.component';


const googleLoginOptions: LoginOpt = {
  scope: 'https://www.googleapis.com/auth/calendar.events'
};

const authConfig = new AuthServiceConfig([
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.facebookAuthKey)
  },
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(environment.googleAuthKey, googleLoginOptions)
  },
]);

@NgModule({
  declarations: [
    ForgotPasswordComponent,
    LoginComponent,
    RegistrationComponent,
    EmailComponent,
    InformationComponent,
    AvatarComponent,
    SocialButtonsComponent,
    FlyingPlaneDirective,
    ProfileMoreInfoComponent
  ],
  imports: [
    CommonModule,
    GooglePlaceModule,
    SocialLoginModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  exports: [

  ],
  entryComponents: [
    ForgotPasswordComponent,
    LoginComponent,
    RegistrationComponent,
    ProfileMoreInfoComponent
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: () => authConfig
    },
  ]
})
export class AuthModule { }
