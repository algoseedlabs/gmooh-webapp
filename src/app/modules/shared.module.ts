import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { PreloaderComponent } from '../components/public/shared/preloader/preloader.component';
import { RoundUpPipe } from '../pipes/round-up.pipe';
import { DayEventOnHoverDirective } from '../directives/day-event.directive';
import { SocialSharingComponent } from '../components/public/shared/social-sharing/social-sharing.component';
import { ShareModule } from '@ngx-share/core';

@NgModule({
  declarations: [
    PreloaderComponent,
    RoundUpPipe,
    DayEventOnHoverDirective,
    SocialSharingComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ShareModule,
  ],
  exports: [
    PreloaderComponent,
    RoundUpPipe,
    DayEventOnHoverDirective,
    ReactiveFormsModule,
    FormsModule,
    SocialSharingComponent
  ],
  entryComponents: [

  ]
})
export class SharedModule { }
