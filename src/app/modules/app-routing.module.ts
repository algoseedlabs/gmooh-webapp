import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from '../components/public/main/main.component';
import { RecommendationComponent } from '../components/public/recommendation/recommendation.component';
import { EventComponent } from '../components/public/event/event.component';
import { CityComponent } from '../components/public/city/city.component';
import { CityGuard } from '../guards/city.guard';
import { EventGuard } from '../guards/event.guard';
import { BookingDetailsComponent } from '../components/public/booking-details/booking-details.component';
import { AuthGuard } from '../guards/auth.guard';
import { BookingDetailsGuard } from '../guards/booking-details.guard';
import { TripDetailsComponent } from '../components/public/trip-details/trip-details.component';
import { PrivacyComponent } from '../components/public/privacy/privacy.component';
import { HomeComponent } from '../components/public/home/home.component';



const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'main', component: MainComponent},
  { path: 'recommendation', component: RecommendationComponent },
  { path: 'event', canActivate: [EventGuard], component: EventComponent },
  { path: 'city',  canActivate: [CityGuard], component: CityComponent },
  { path: 'booking-details', canActivate: [BookingDetailsGuard], component: BookingDetailsComponent },
  { path: 'booking-details/:id', component: BookingDetailsComponent },
  { path: 'booking-details/copy-trip/:copyTripId', component: BookingDetailsComponent },
  { path: 'trip-details/:id', component: TripDetailsComponent },
  { path: 'account/:tab', canActivate: [AuthGuard], loadChildren: './user.module#UserModule' },
  { path: 'privacy', component: PrivacyComponent },
  { path: '**', redirectTo: 'main' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
