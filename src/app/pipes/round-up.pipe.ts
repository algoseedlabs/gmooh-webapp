import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roundUp'
})
export class RoundUpPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return Number.parseFloat(value).toFixed(1);
  }

}
