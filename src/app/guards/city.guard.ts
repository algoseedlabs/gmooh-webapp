import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CityGuard implements CanActivate {

  constructor(private apiService: ApiService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      /*if (this.apiService.targetDestination.id) {
        return true;
      }
      this.router.navigate(['/main']);*/
    return true;

  }
}
