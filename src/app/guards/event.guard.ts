import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {SearchService} from '../services/search.service';

@Injectable({
  providedIn: 'root'
})
export class EventGuard implements CanActivate {

  constructor(
    private search: SearchService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if (this.search.flightsData.id) {
        return true;
      }
      this.router.navigate(['/main']);
  }
}
