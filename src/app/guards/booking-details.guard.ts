import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {ApiService} from '../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class BookingDetailsGuard implements CanActivate {

  constructor(private apiService: ApiService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const bookingData = Object.keys(this.apiService.reservationData).length > 0;
    if (bookingData) {
      return true;
    }
    this.router.navigate(['/main']);
  }
}
