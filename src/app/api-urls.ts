import { environment } from '../environments/environment';
const { BASE_URL } = environment;
export const URLS = {
  flights: BASE_URL + '/api/search/flights',
  location: BASE_URL + '/api/search/location/',
  recommendation: BASE_URL + '/api/search/recommendation',
  cities: BASE_URL + '/api/cities/city',
  citiesId: BASE_URL + '/api/cities/city/concrete',
  cityPlaces: (cityId: number) => BASE_URL + `/api/cities/${cityId}/place/`,
  searchCityPlaces: BASE_URL + '/api/cities/place/search/',
  sortByCityPlaces: BASE_URL + '/api/cities/places/sortBy/',
  cityMorePlaces: BASE_URL + '/api/cities/city/showMorePlaces',
  cityMoreEvents: BASE_URL + '/api/cities/city/showMoreEvents',
  cityEvents: BASE_URL + '/api/cities/events/',
  landmarks: BASE_URL + '/api/cities/city/',
  welcome: BASE_URL + '/api/promo',
  createTrip(pending) { return pending ?
                        BASE_URL + '/api/trips/pending' :
                        BASE_URL + '/api/trips/create'
                      },
  getTripById: BASE_URL + '/api/trips/trip/',

  tripList: (id, page, size) => (BASE_URL + `/api/trips/user?page=${page}&size=${size}&userId=${id}`),
  singleTrip: BASE_URL + '/api/trips/trip/',
  singleSecureTrip: BASE_URL + '/api/trips/trip-secure/',
  registration: BASE_URL + '/api/registration',
  avatar: BASE_URL + '/api/users/uploadAvatar',
  changeAvatar: BASE_URL + '/api/users/changeAvatar',
  login: BASE_URL + '/api/auth',
  getProfile: BASE_URL + '/api/users/',
  editProfile: BASE_URL + '/api/users/edit',
  reviews: BASE_URL + '/api/review/',
  sendReview: BASE_URL + '/api/review/review',
  sendComment: BASE_URL + '/api/review/comment',
  forgotPassword : BASE_URL + '/api/password/forgot',
  likeReview : BASE_URL + '/api/review/review/like/',
  dislikeReview : BASE_URL + '/api/review/review/dislike/',
  likeComment : BASE_URL + '/api/review/comment/like/',
  dislikeComment : BASE_URL + '/api/review/comment/dislike/',
  likeTrip : BASE_URL + '/api/trips/like/',
  loginWithSocial : BASE_URL  + '/api/social/auth',

  editMetaTags: 'https://gmooh.app/edit-meta'
};
