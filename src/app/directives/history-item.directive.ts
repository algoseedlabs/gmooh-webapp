import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appHistoryItem]',
  exportAs: 'historyItem'
})

export class HistoryItemDirective {

  @HostBinding('style.opacity') opacity;

  constructor() {
  }

  toggleOpacity(opacity): void {
    this.opacity = opacity;
  }



}
