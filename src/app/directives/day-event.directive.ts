import { Directive, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appDayEventOnHover]',
  exportAs: 'dayEventOnHover'
})

export class DayEventOnHoverDirective {

  @HostBinding('style.opacity') opacity;

  constructor() {
  }

  toggleOpacity(opacity): void {
    this.opacity = opacity;
  }


}
