import {Directive, ElementRef, HostBinding, Input } from '@angular/core';
import { interval, timer } from 'rxjs';
import { DirectivesService } from '../services/directives.service';

@Directive({
  selector: '[appFlyingPlane]'
})

export class FlyingPlaneDirective {

  @HostBinding('style.left ') left ;

  constructor(
    private el: ElementRef,
    private directivesService: DirectivesService
  ) {
  }

  @Input() set appFlyingPlane(data) {
    const $ = {
      radius  : 170,
      speed   : 20
    };
    this.directivesService.resetTraces();

    if (data === 'right') {
      let f = 0;
      const s = 2 * Math.PI / 160;

      const elem = this.el.nativeElement;
      const tick = interval($.speed);
      let count = 0;
      let deg = 190;
      const tickSubscription = tick.subscribe(n => {
        count += 1;
        deg -= 2;
        f += s;
        const left = 300 + $.radius * Math.sin(f);
        const top = 40 + $.radius * Math.cos(f);
        elem.style.left = left  + 'px';
        elem.style.top = top + 'px';
        elem.style.transform = `rotate(${deg}deg)`;
        this.directivesService.rightTrace = { left: left, top: top };
        if (count === 70) {
          tickSubscription.unsubscribe();
        }
      });
    }

    if (data === 'left') {
      const elem = this.el.nativeElement;
      const tick = timer(1000, $.speed);
      let count = 0;
      const coords = {
        left: 0,
        top: 60
      };
      const tickSubscription = tick.subscribe(n => {
        count += 1;
        coords.left += 5;
        elem.style.left = coords.left + 'px';
        // elem.style.top = coords.top + 'px';
        this.directivesService.leftTrace = {...coords};
        if (count === 30) {
          tickSubscription.unsubscribe();
        }
      });
    }

  }


}
