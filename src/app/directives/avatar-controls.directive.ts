import {Directive, ElementRef, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appAvatarControls]',
})

export class AvatarControlsDirective {


  constructor(private el: ElementRef) {
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.toggleOffset(0);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.toggleOffset(302);
  }

  toggleOffset(offset): void {
    this.el.nativeElement.children[0].style.top = offset + 'px';
  }



}
