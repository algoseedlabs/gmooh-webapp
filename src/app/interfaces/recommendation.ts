export interface Recommendation {
  dateFrom: string;
  dateTo: string;
  originPlaceId: string;
  destinationPlaceId?: string;
}
