// оригинал
// export interface IPlaceData {
//   address: string; // can be removed
//   images?: string[]; // can be removed
//   image?: string; // can be removed
//   latitude: number;
//   longitude: number;
//   name: string;
//   openingHours?: IOpeningHours;
//   placeId: string;
//   rating?: number; // can be removed
//   reviews?: number; // can be removed
//   type: string;
//   categoryName?: string;
//   description?: string; // can be removed
//   websiteLink?: string; // can be removed
//   phoneNumber: string; // can be removed
//   startDate?: string;
//   endDate?: string;
// }



export interface IPlaceData {
  name?: string;
  placeName?: string;
  placeId: string;
  type?: number;
  placeType?: string;
  address?: string; // can be removed
  images?: string[]; // can be removed
  image?: string; // can be removed
  latitude?: number;
  longitude?: number;
  openingHours?: IOpeningHours;
  rating?: number; // can be removed
  reviews?: number; // can be removed
  categoryName?: string;
  description?: string; // can be removed
  websiteLink?: string; // can be removed
  phoneNumber?: string; // can be removed
  startDate?: string;
  endDate?: string;
}

export interface IOpeningHours {
  openNow: boolean;
  weekDayHours: string[];
}


// При переходе на новую страницу
export interface IPlaceInfo {
  image: string;
  latitude: number;
  longitude: number;
  name: string;
  placeId: string;
  rating?: number;
  reviews?: number;
  date?: string;
  genreName?: string;
}







export interface ITripsCreate {
  city: string;
  dateFrom: string;
  dateTo: string;
  destinationPlaceId: string;
  detailsList: IPlaceDetails[];
  image: string; // can be removed
  price: number;
  transportType: string;
}

export interface IPlaceDetails {
  address: string; // can be removed
  dateFrom: string;
  dateTo: string;
  eventType: string;
  image: string; // can be removed
  name: string;
  placeId: string;
  price: number;
}
