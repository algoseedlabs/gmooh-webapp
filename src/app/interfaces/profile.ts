export class Profile {

  country: string;
  dateOfBirth: string;
  firstName: string;
  gender: string;
  lastName: string;
  phoneNumber: number;
  userId: number;
}
