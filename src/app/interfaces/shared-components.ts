import { IOpeningHours } from './places';

export interface IItemOfListOfNamesToSort {
  id: number;
  name: string;
  active: boolean;
}

export interface ISearchItemError {
  errorText: string;
  class?: string;
}

export interface ISearchItem {
  address?: string;
  name: string;
  placeId: string;
  type: number;
  categoryName?: string;
  openingHours?: IOpeningHours;
  startDate?: string;
  endDate?: string;
  alreadyInBasket?: boolean;
  image?: string;
}

export interface IOptionsForSearchPlaces {
  query: string;
  cityId: number;
  type: number;
  dateFrom: string;
  dateTo: string;
}

export interface IOptionsForSortByCityPlaces {
  cityId: number;
  type: number;
  sortBy: number;
  nextPageToken: string;
  dateFrom: string;
  dateTo: string;
}
