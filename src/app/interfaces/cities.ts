export interface ISection {
  id: number;
  title: string;
  color: string;
  googleColorId: number;
  imgTitle: string;
  data?: any;
}
