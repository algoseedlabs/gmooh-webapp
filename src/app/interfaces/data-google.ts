export interface IDataGoogle {
  authToken: string;
  email: string;
}

export interface IGoogleEvent {
  summary: string;
  start: { dateTime: any };
  end: { dateTime: any };
  location: string;
  description: string;
  colorId: number;
}
