export interface Review {

  mark: number,
  text: string,
  tripId: number

}
