export interface Email {
  email: string;
  subscribe: boolean;
}
