export interface Trip {
  dateFrom: string;
  dateTo: string;
  destinationPlaceId: string;
  detailsList: Array<object>;
  originPlaceId: string;
  price: number;
  transportType: string;
}
