export interface Comment {
      mark: number,
      reviewId: number,
      text: string,
      userId: number,
}
