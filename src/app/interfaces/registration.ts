export interface Registration {
  birthday?: string;
  country?: string;
  email: string;
  firstName: string;
  gender?: number;
  lastName?: string;
  password: number;
  phoneNumber?: string;
  receiveMsg?: boolean;
  agreement: boolean;
}
