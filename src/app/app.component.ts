import { Component, OnInit, HostListener } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { WindowService } from './services/window.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  hiddenHeader = true;
  hiddenFooter = true;
  footerShownBtnText = 'About us and contact info';
  footerHiddenBtnText = 'Hide';
  footerToggleBtnText = this.footerShownBtnText;

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event): void {
    const offset = {
      offsetY: window.pageYOffset,
    };
    this.windowService.pageOffsetY.next(offset);
  }

  @HostListener('window:resize', ['$event'])
  onWindowResize(event): void {
    const size = {
      width: window.innerWidth,
      height: window.innerHeight
    };
    this.windowService.windowSize.next(size);
  }

  constructor(
    private router: Router,
    private windowService: WindowService
    ) {}


  ngOnInit() {
    this.handleHeaderVisibility(this.router.url);
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.handleHeaderVisibility(val.url);
      }
    });
  }

  handleHeaderVisibility(url) {
    this.hiddenHeader = url === '/' || url === '/main';
  }

  onActivate(event) {
    window.scroll(0, 0 );
  }

  toggleFooter() {
    this.hiddenFooter = !this.hiddenFooter;
    this.setFooterToggleBtnText();
  }

  setFooterToggleBtnText() {
    this.footerToggleBtnText = this.footerToggleBtnText === 'Hide' ? this.footerShownBtnText : this.footerHiddenBtnText;
  }

}
