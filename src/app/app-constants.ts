
export const SUCCESS_MODAL_WIDTH = '25vw';
export const SUCCESS_MODAL_MOBILE = '70vw';



export const SECTIONS = [
  {
    id: 1,
    title: 'Accommodation',
    color: '#d9b3ff',
    googleColorId: 1,
    imgTitle: 'icon-accomadation.png',
    data: null
  },
  {
    id: 2,
    title: 'Restaurant',
    color: '#00cc00',
    googleColorId: 2,
    imgTitle: 'icon-restaurants.png',
    data: null
  },
  {
    id: 3,
    title: 'Shop',
    color: '#ffcc66',
    googleColorId: 5,
    imgTitle: 'icon-shopping.png',
    data: null
  },
  {
    id: 4,
    title: 'Landmark',
    color: '#ff33cc',
    googleColorId: 4,
    imgTitle: 'icon-landmarks.png',
    data: null
  },
  {
    id: 5,
    title: 'Event',
    color: '#ff99bb',
    googleColorId: 5,
    imgTitle: 'icon-events.png',
    data: null
  },
];

export const LIST_OF_NAMES_TO_SORT = [
  { id: 1, name: 'A-Z', active: false },
  { id: 2, name: 'Z-A', active: false },
  { id: 3, name: 'High ratings', active: true },
  { id: 4, name: 'Low ratings', active: false }
];


export const RECEIPT_LIST = {
  1: {
    quantity: 0,
    price: 50
  },
  2: {
    quantity: 0,
    price: 50
  },
  3: {
    quantity: 0,
    price: 50
  },
  4: {
    quantity: 0,
    price: 50
  },
  5: {
    quantity: 0,
    price: 50
  },
};
