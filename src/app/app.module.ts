import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { DeferLoadModule } from '@trademe/ng-defer-load';
import { MaterialModule } from './modules/material.module';
import { AppRoutingModule } from './modules/app-routing.module';
import { SharedModule } from './modules/shared.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/public/main/main.component';
import { SearchFormComponent } from './components/public/search-form/search-form.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CityGuard} from './guards/city.guard';
import { EventGuard } from './guards/event.guard';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { PaymentModalComponent } from './components/user/payment-modal/payment-modal.component';
import { AuthGuard } from './guards/auth.guard';
import { BookingDetailsGuard } from './guards/booking-details.guard';
import { WelcomeGuard } from './guards/welcome.guard';
import { HeaderComponent } from './components/public/header/header.component';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { FooterComponent } from './components/public/footer/footer.component';
import { SuccessComponent } from './components/public/@dialogs/success/success.component';
import { ErrorComponent } from './components/public/@dialogs/error/error.component';
import { PrivacyComponent } from './components/public/privacy/privacy.component';
import { HomeComponent } from './components/public/home/home.component';
import { LoginButtonComponent } from './components/public/shared/login-button/login-button.component';
import { VideoComponent } from './components/public/shared/video/video.component';
import { DemoPage1Component } from './components/public/home/demo-page1/demo-page1.component';
import { DemoPage2Component } from './components/public/home/demo-page2/demo-page2.component';
import { DemoPage3Component } from './components/public/home/demo-page3/demo-page3.component';
import { DemoPage4Component } from './components/public/home/demo-page4/demo-page4.component';
import { AuthModule } from './modules/auth.module';
import { BookingModule } from './modules/booking.module';
import { ShareModule } from '@ngx-share/core';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SearchFormComponent,
    WelcomeComponent,
    PaymentModalComponent,
    SuccessComponent,
    HeaderComponent,
    FooterComponent,
    ErrorComponent,
    PrivacyComponent,
    HomeComponent,
    LoginButtonComponent,
    VideoComponent,
    DemoPage1Component,
    DemoPage2Component,
    DemoPage3Component,
    DemoPage4Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    FlexLayoutModule,
    DeferLoadModule,
    BrowserAnimationsModule,
    DeviceDetectorModule.forRoot(),
    SharedModule,
    AuthModule,
    BookingModule,
    ShareModule
  ],
  entryComponents: [
    PaymentModalComponent,
    SuccessComponent,
    ErrorComponent,
  ],
  providers: [
    CityGuard,
    EventGuard,
    AuthGuard,
    BookingDetailsGuard,
    WelcomeGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
