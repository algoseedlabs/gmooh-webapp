import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  public emailGroup;
  public emailValid = false;
  public formWasSubmitted = false;
  public errorMessage = '';
  public regexp = "^(([^<>()[\\]\\\\.,;:\\s@\\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";

  constructor(
    private router: Router,
    private apiService: ApiService,
  ) {}

  ngOnInit() {
    this.emailGroup = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(this.regexp)]),
      subscribe: new FormControl()
    });
    this.emailGroup.controls.email.statusChanges.subscribe( res => {
      this.emailValid = res === 'VALID';
    });

  }

  onSubmitForm() {
    this.formWasSubmitted = true;
    if (this.emailValid) {
      this.apiService.sendEmail(this.emailGroup.value).subscribe(
        res => {
          if (res.status === 'SUCCESS') {
            this.errorMessage = '';
            localStorage.setItem('@gmooh:token', res.response.token)
            this.router.navigate(['main']);
          } else {
            this.emailValid = false;
            this.errorMessage = res.errorMessage;
          }
        },
        err => {
          console.log(err);
        }
      );

    }
  }
}



