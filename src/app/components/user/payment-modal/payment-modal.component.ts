import { Component, Inject, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { BookingDetailsComponent } from '../../public/booking-details/booking-details.component';
import { ApiService } from '../../../services/api.service';
import { environment } from '../../../../environments/environment';
import { BookingDetailsService } from '../../../services/booking/booking-details.service';
import {SearchService} from '../../../services/search.service';


@Component({
  selector: 'app-payment-modal',
  templateUrl: './payment-modal.component.html',
  styleUrls: [ './payment-modal.component.css' ]
})
export class PaymentModalComponent implements OnInit {

  private user = JSON.parse(localStorage.getItem('@gmooh:currentUser'));
  public baseUrl = environment.BASE_URL;
  public regExpNum = '^(0)$|^([1-9][0-9]*)$';
  public regExpNeme = '^[a-zA-Z\'][a-zA-Z-\' ]+[a-zA-Z\']?$';
  public hotel = '';
  public paymentForm;
  public nameCreditCardValid;
  public creditCardValid;
  public expiryDateMonthValid;
  public expiryDateYearValid;
  public numberCvcValid;
  public submitButtonClicked = false;



  constructor(
    public dialogRef: MatDialogRef<BookingDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private api: ApiService,
    public dialog: MatDialog,
    public booking: BookingDetailsService,
    private search: SearchService,
  ) {}


  ngOnInit() {
    this.booking.resetEventsQuantityAndTotalSum();
    this.booking.calculateEventsQuantity();
    this.booking.calculateTotalSum();

    this.paymentForm = new FormGroup({
      nameCreditCard: new FormControl('', [ Validators.required, Validators.pattern(this.regExpNeme) ]),
      cardNumber: new FormControl('', [ Validators.required, Validators.minLength(16), Validators.maxLength(16), Validators.pattern(this.regExpNum) ]),
      dateMonth: new FormControl('', [ Validators.required, Validators.min(1), Validators.max(12), Validators.pattern(this.regExpNum) ]),
      dateYear: new FormControl('', [ Validators.required, Validators.min(2000), Validators.max(2070), Validators.minLength(4), Validators.maxLength(4), Validators.pattern(this.regExpNum) ]),
      cvcCode: new FormControl('', [ Validators.required, Validators.minLength(3), Validators.maxLength(3), Validators.pattern(this.regExpNum) ]),
    });

    this.nameCreditCardValid = this.paymentForm.controls.nameCreditCard.status === 'VALID';
    this.creditCardValid = this.paymentForm.controls.cardNumber.status === 'VALID';
    this.expiryDateMonthValid = this.paymentForm.controls.dateMonth.status === 'VALID';
    this.expiryDateYearValid = this.paymentForm.controls.dateYear.status === 'VALID';
    this.numberCvcValid = this.paymentForm.controls.cvcCode.status === 'VALID';

    this.paymentForm.controls.nameCreditCard.statusChanges.subscribe(res => {
      this.nameCreditCardValid = res === 'VALID';
    });
    this.paymentForm.controls.cardNumber.statusChanges.subscribe(res => {
      this.creditCardValid = res === 'VALID';
    });
    this.paymentForm.controls.dateMonth.statusChanges.subscribe(res => {
      this.expiryDateMonthValid = res === 'VALID';
    });
    this.paymentForm.controls.dateYear.statusChanges.subscribe(res => {
      this.expiryDateYearValid = res === 'VALID';
    });
    this.paymentForm.controls.cvcCode.statusChanges.subscribe(res => {
      this.numberCvcValid = res === 'VALID';
    });
  }

  onSubmitForm() {
    if (this.user && this.user.token) {
      this.api.createTrip(this.booking.getTripData(), this.user.token).subscribe(res => {
        if (res.status === 'SUCCESS') {
          setTimeout(() => {
            this.dialogRef.close();
            this.booking.clearCacheAndRedirect();
          }, 1000);
          this.booking.openPaymentResponseModal(false, 'Trip added successfully!');
        } else {
          this.dialogRef.close();
          this.booking.openPaymentResponseModal(true, 'Trip not added. Please try later.');
        }
      });
    } else {
      this.dialogRef.close();
      this.booking.openLoginModal();
    }

  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

}
