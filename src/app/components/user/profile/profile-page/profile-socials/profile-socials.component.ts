import { Component, OnInit, Input } from '@angular/core';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { AuthService as Auth } from '../../../../../services/auth.service';

@Component({
  selector: 'app-profile-socials',
  templateUrl: './profile-socials.component.html',
  styleUrls: ['./profile-socials.component.css', '../profile-page.component.css']
})
export class ProfileSocialsComponent implements OnInit {

  facebookBtnChecked: boolean;
  googleBtnChecked: boolean;
  socialTokens;
  @Input() socialNetworkName;

  constructor(
    private authService: AuthService,
    private auth: Auth
  ) { }

  ngOnInit() {
    this.getSocialTokens();
  }

  getSocialTokens(): void {
    this.socialTokens = this.auth.socialAccountToken;
    const { google, facebook } = this.socialTokens;
    this.googleBtnChecked = !!google;
    this.facebookBtnChecked = !!facebook;
  }

  bindFacebookAccount(): void {
    if (!this.socialTokens.facebook) {
      this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(res => {
        this.setSocialAccountData(res);
      });
    }
  }

  bindGoogleAccount(): void {
    if (!this.socialTokens.google) {
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(res => {
        this.setSocialAccountData(res);
      });
    }
  }

  setSocialAccountData(res) {
    const accountName = res.provider.toLocaleLowerCase();
    this.auth.socialAccountToken = { [accountName]: res.authToken };
    this.googleBtnChecked = true;
    this.socialTokens = this.auth.socialAccountToken;
  }

}
