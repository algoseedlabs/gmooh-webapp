import { Component, Input, OnInit } from '@angular/core';
import { AvatarService } from '../../../../../services/user/avatar.service';
import { UserService } from '../../../../../services/user/user.service';

@Component({
  selector: 'app-profile-avatar',
  templateUrl: './profile-avatar.component.html',
  styleUrls: ['./profile-avatar.component.css', '../profile-page.component.css']
})
export class ProfileAvatarComponent implements OnInit {

  @Input() userProfile;
  @Input() user;

  constructor(
    public avatar: AvatarService,
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  onChooseAvatar(event: Event): void {
    this.userService.onChooseAvatar(event, result => {
      this.avatar.avatarSrc = result.toString();
      this.avatar.formattedAvatar = this.avatar.avatarSrc.split('base64,')[1];
    });
  }

}
