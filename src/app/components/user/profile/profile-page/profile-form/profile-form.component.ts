import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { UserService } from '../../../../../services/user/user.service';
import { AvatarService } from '../../../../../services/user/avatar.service';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css', '../profile-page.component.css']
})
export class ProfileFormComponent implements OnInit, OnChanges {

  nameRegex = new RegExp('^[A-Z][a-z]*$');
  phoneRegex = new RegExp('^[\\d]{6,20}$');
  nameErrorText = 'name must contain first capital letter A-Z and letters a-z without whitespaces.';

  @Input() userProfile;

  constructor(
    public user: UserService,
    public avatar: AvatarService,
  ) { }

  personalData = new FormGroup({
    firstName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
      Validators.pattern(this.nameRegex)
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
      Validators.pattern(this.nameRegex)
    ]),
    dateOfBirth: new FormControl('', [ Validators.required ]),
    gender: new FormControl('', []),
    phoneNumber: new FormControl('', [ Validators.pattern(this.phoneRegex) ]),
    country: new FormControl('', ),
    userId: new FormControl(''),
  });
  formattedData;

  ngOnInit() {
  }

  ngOnChanges(changes) {
    const { previousValue, currentValue } = changes.userProfile;
    if (
      previousValue === undefined &&
      currentValue !== undefined
    ) {
      if (!currentValue.gender || currentValue.gender === 'undefined') {
        changes.userProfile.currentValue.gender = this.user.genders[0];
      } else {
        setTimeout(_ => {
          this.avatar.setAvatarUrl(currentValue);
        }, 0);
      }
      this.personalData.patchValue(currentValue);
    }
  }

  handleAddressChange(address: Address): void {
    this.personalData.controls.country.setValue(address.formatted_address);
  }

  prepareDataForUpdate() {
    this.formattedData = this.user.prepareDataForSave(this.personalData.value);
    this.formattedData.userId = this.user.userData.id;
    this.formattedData.dateOfBirth = moment(this.formattedData.dateOfBirth).format('MM/DD/YYYY');
  }

}

