import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from 'src/app/services/user/user.service';
import { AuthService as Auth } from '../../../../services/auth.service';
import { WindowService } from 'src/app/services/window.service';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: [ './profile-page.component.css' ]
})

export class ProfilePageComponent implements OnInit {

  user;
  userProfile;
  socialNetworkName;
  isLoading = true;
  @ViewChild('profile') profile;

  constructor(
    private apiService: ApiService,
    private userService: UserService,
    private auth: Auth,
    private windowService: WindowService
  ) {}

  accountData = new FormGroup({
    email: new FormControl({ value: '', disabled: true }),
  });

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    this.user = this.userService.userData;
    console.log(this.user)
    if (!this.user) {
      this.isLoading = false;
      setTimeout(() => {
        this.scrollToFirstElement();
      }, 0);
      return;
    }

    if (this.user.socialProvider) {
      this.socialNetworkName = this.user.socialProvider.toLowerCase();
    }

    this.apiService.getUser(this.user.id, this.user.token).subscribe(data => {
      this.isLoading = false;
      data.response.dateOfBirth = new Date(data.response.dateOfBirth);
      this.userProfile = data.response;
      this.accountData.controls.email.setValue(data.response.email);
      this.scrollToFirstElement();
    });
  }

  forgotPassword() {
    const email = { email: this.userProfile.email };
    this.auth.sendForgotPassRequest(email);

  }

  private scrollToFirstElement() {
    if (!this.windowService.isDesktop) {
      const scrollTo = this.profile.nativeElement.offsetTop;
      window.scrollTo(0, scrollTo);
    }
  }
}


