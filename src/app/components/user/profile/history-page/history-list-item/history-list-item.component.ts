import { Component, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router, ActivatedRoute  } from '@angular/router';
import { trigger, transition, state, style, animate } from '@angular/animations';
import { environment } from '../../../../../../environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PreviewTripComponent } from '../preview-trip/preview-trip.component';
import { MatDialog } from '@angular/material';


@Component({
  selector: 'app-history-list-item',
  templateUrl: './history-list-item.component.html',
  styleUrls: ['./history-list-item.component.css'],
  animations: [
    trigger('showItem', [
      state('hidden', style({
        opacity: 0
      })),
      state('shown', style({
        opacity: 1,
        transform: 'translateY(-10px)'
      })),
      transition('* => *', [animate('0.3s')])
    ]),
  ]
})

export class HistoryListItemComponent {

  @Input() index;
  @Input() data;
  @Input() isMobile;
  @Input() hiddenState;
  baseUrl = environment.BASE_URL;
  defaultImg = 'assets/images/not-img.png';
  shareIcon = 'assets/images/share-icon.png';

  constructor(
    private apiService: ApiService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    ) { }

  goToTripDetails(id) {
    const targetUrl = this.data.status === 'pending' ? '/booking-details' : '/trip-details';
    this.router.navigate([targetUrl, id]);
  }

  onCopyLink(id, event) {
    event.stopPropagation();
    this.writeToBuffer(id);
  }

  writeToBuffer(id) {
    const shareLink = window.location.host + `/trip-details/${id}`;
    const buffer: any = navigator;
    buffer.clipboard.writeText(shareLink);
    this._snackBar.open('Link copied', '', {
      duration: 1000,
    });
  }

  onClickPreview(event, data) {
    event.stopPropagation();
    this.openPreviewDialog(data);
  }

  openPreviewDialog(data): void {
    const dialogRef = this.dialog.open(PreviewTripComponent, {
      width: '50vw',
      data: data.detailsList,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.writeToBuffer(data.id);
      }
    });
  }
}

