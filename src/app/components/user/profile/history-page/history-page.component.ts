import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {DeviceDetectorService} from 'ngx-device-detector';
import {ApiService} from 'src/app/services/api.service';
import {UserService} from 'src/app/services/user/user.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {ActivatedRoute, Router} from '@angular/router';
import {WindowService} from 'src/app/services/window.service';


@Component({
    selector: 'app-history-page',
    templateUrl: './history-page.component.html',
    styleUrls: ['./history-page.component.css'],
    animations: [
        trigger('showNode', [
            state('hidden', style({
                opacity: 0
            })),
            state('shown', style({
                opacity: 1,
                transform: 'translateY(-10px)'
            })),
            transition('* => *', [animate('0.4s')])
        ]),

    ]
})
export class HistoryPageComponent implements OnInit, OnDestroy {
    
    subscriberQueryParams;
    isMobile = false;
    historyData;
    hiddenNodeItem = true;
    startOffsetTop = 65;
    tripNodeHeight = 130;

    isLoading = true;

    page = 1;
    size = 10;
    totalItems = 0;

    @ViewChild('history') history;

    constructor(
        private deviceDetectorService: DeviceDetectorService,
        private apiService: ApiService,
        private userService: UserService,
        private router: Router,
        private route: ActivatedRoute,
        private windowService: WindowService
    ) {
    }

    ngOnInit() {
        this.isMobile = this.deviceDetectorService.isMobile();
        this.subscriberQueryParams = this.route.queryParams.subscribe(params => {
            if(params && !params.page) { return; };
            this.page = params.page ? +params.page : 1;
            this.showHistoryPage(this.page);
        });
        
        if(!this.route.snapshot.queryParams.page) {
            this.showHistoryPage(this.page);
        }
    }

    ngOnDestroy(): void {
        if(this.subscriberQueryParams) {
            this.subscriberQueryParams.unsubscribe();
        }
    }

    public handlePage(e: any) {
        this.isLoading = true;

        this.page = e.pageIndex + 1;
        this.size = e.pageSize;
        this.historyData = [];

        this.navigateByPage();
    }

    showHistoryPage(page) {
        const {id, token} = this.userService.userData;

        this.apiService.getTripList(id, page, this.size, token).subscribe(data => {
                if (data.status === 'SUCCESS') {
                    const historyPage = data.response.content;
                    this.totalItems = data.response.totalElements;
                    this.isLoading = false;

                    this.historyData = historyPage;
                    this.hiddenNodeItem = false;

                    this.scrollToFirstElement();
                } else {
                    this.isLoading = false;
                    this.scrollToFirstElement();
                    this.handleError();
                }
            },
            err => {
                this.isLoading = false;

                if (err.status === 404) {
                    this.handleError();
                }
            });
    }

    handleError() {
        this.historyData = [];
    }

    private scrollToFirstElement() {
        if (!this.windowService.isDesktop) {
            const scrollTo = this.history.nativeElement.offsetTop;
            window.scrollTo(0, scrollTo);
        } else {
            window.scroll(0, 0);
        }
    }

    private navigateByPage(): void {
        const queryParams = {
            page: this.page,
        };
        this.router.navigate(['.'], {relativeTo: this.route, queryParams: queryParams});
    }
}

