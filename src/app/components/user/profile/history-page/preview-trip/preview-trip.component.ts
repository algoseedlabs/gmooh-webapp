import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { SECTIONS } from '../../../../../app-constants';

@Component({
  selector: 'app-preview-trip',
  templateUrl: './preview-trip.component.html',
  styleUrls: ['./preview-trip.component.css']
})
export class PreviewTripComponent implements OnInit {

  sections = SECTIONS;
  days = [];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}

  ngOnInit() {
    this.data.forEach((el, index) => {

      if (typeof el.type === 'number') {
        el.type = this.sections.find(item => item.id === el.type).title;
      }
      const date = el.dateFrom.split(' ');

      el['time'] = this.parseTime(date[1]);

      if (index === 0) {
        this.days.push({
          date: date[0],
          events: [ el ]
        });
      } else {
        let dateExists = false;
        let dateIndex = 0;
        this.days.forEach((item, key) => {
          if (item.date === date[0]) {
            dateExists = true;
            dateIndex = key;
          }
        });
        if (dateExists) {
          this.days[dateIndex].events.push(el);
        } else {
          this.days.push({
            date: date[0],
            events: [ el ]
          });
        }
      }

    });
  }

  closeModal() {
    this.dialogRef.close(false);
  }

  onClickSave() {
    this.dialogRef.close(true);
  }

  parseTime(time) {
    const timeArr = time.split(':');
    if (timeArr[0].length < 2) {
      timeArr[0] = '0' + timeArr[0];
    }
    return  timeArr.join(':').slice(0, 5);
  }

}
