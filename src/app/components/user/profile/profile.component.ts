import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { WindowService } from '../../../services/window.service';


const NAV_MOBILE_WIDTH = '100vw';
const NAV_WIDTH = '40vw';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  animations: [
    trigger('moveBorder', [
      state('onProfile', style({
        left: 'calc((100vw - {{width}}) / 2)',
      }), { params: { width: '60vw' } }),
      state('onHistory', style({
        left: 'calc((100vw - {{width}}) / 2 + ({{width}} / 2))',
      }), { params: { width: '60vw' } }),
      transition('* => *', [
        animate('0.2s')
      ])
    ])
  ],
})
export class ProfileComponent implements OnInit {


  activeColor = '#00919a';
  tabs = [{ title: 'Profile page', slug: 'profile' }, { title: 'History page', slug: 'history' }];
  currentPage;
  navWidth = NAV_WIDTH;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private window: WindowService
  ) { }

  ngOnInit() {
    this.navWidth = !this.window.isDesktop ? NAV_MOBILE_WIDTH : NAV_WIDTH;
    this.activeTab = this.activeTab;
  }

  setPage(page): void {
    this.router.navigate(['account/' + page.slug], { });
    this.activeTab = page.slug;

  }

  set activeTab(active) {
    this.currentPage = this.tabs.find(el => el.slug === active);
  }

  get activeTab() {
    return this.route.snapshot.paramMap.get('tab');
  }

  get currentUrl() {
    let url = this.route.snapshot.paramMap.get('tab');
    return url;
  }
}
