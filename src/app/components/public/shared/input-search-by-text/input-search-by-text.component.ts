import { Component, Input } from '@angular/core';
import { IOptionsForSearchPlaces, ISearchItem, ISearchItemError } from '../../../../interfaces/shared-components';
import { EventPageService } from '../../../../services/event-page.service';
import { IPlaceInfo } from '../../../../interfaces/places';
import { ApiService } from '../../../../services/api.service';
import { ReservationService } from '../../../../services/reservation.service';


@Component({
  selector: 'app-input-search-by-text',
  templateUrl: './input-search-by-text.component.html',
  styleUrls: [ './input-search-by-text.component.css' ]
})

export class InputSearchByTextComponent {
  @Input() max: number;
  @Input() cityId: number;
  @Input() cityPlace: any;
  @Input() placesOfTheSameType: IPlaceInfo[];
  @Input() queryParams;
  @Input() isMobile: boolean;

  basketIcon = 'assets/images/shopping-basket.png';
  searchIcon = 'assets/images/search_gray.png';
  searchList: (ISearchItem | ISearchItemError)[] = [];
  searchText: string;
  searchDelay;

  constructor(
    private eventPageService: EventPageService,
    private reservationService: ReservationService,
    private apiService: ApiService
  ) {}

  onKeyUp(event: any): void {
    if (event.key === 'ArrowDown' || event.key === 'ArrowUp') {
      return;
    }
    clearTimeout(this.searchDelay);
    this.searchDelay = setTimeout(() => {
      this.validateSearch(this.searchText);
    }, 500);
  }

  onClickSearch(): void {
    this.validateSearch(this.searchText);
  }

  validateSearch(text: string): void {
    if (text.length === 0) {
      this.searchList = [];
      return;
    }
    if (text.length >= 2) {
      this.search(text);
    }
  }

  search(query: string): void {
    const dateFrom = this.queryParams.departure;
    const dateTo = this.queryParams.arrival;
    const data: IOptionsForSearchPlaces = {
      query,
      cityId: this.cityId,
      type: this.cityPlace.id,
      dateFrom,
      dateTo
    };

    this.apiService.searchCityPlaces(data).subscribe((response: any) => {
      if (response.status === 'SUCCESS') {
        this.searchList = [];
        if (response.response && response.response.length > 0) {
          const addedPlaces = this.apiService.reservationData;
          response.response.forEach((place) => {
            const dataPlace: ISearchItem = {
              name: place.name,
              placeId: place.placeId,
              // type: this.cityPlace.title.toLowerCase(),
              type: this.cityPlace.id,
              categoryName: place.categoryName || undefined,
              openingHours: place.openingHours || undefined,
              startDate: place.startDate || undefined,
              endDate: place.endDate || undefined,
              alreadyInBasket: !!addedPlaces[place.placeId]
            };
            this.searchList.push(dataPlace);
          });
        } else {
          this.searchList.push({
            errorText: 'Sorry, nothing found.'
          });
        }
      }
    });
  }

  goToEvent(item): void {
    if (item.errorText) {
      return;
    }
    this.eventPageService.goToEvent(
      item.placeId,
      this.placesOfTheSameType,
      this.cityPlace.title,
      this.cityId
    );
  }

  addToCart(event, item: ISearchItem): void {
    event.stopPropagation();
    delete item.alreadyInBasket;
    this.reservationService.eventReservation({...item}, this.isMobile);
    item['alreadyInBasket'] = true;
  }

}
