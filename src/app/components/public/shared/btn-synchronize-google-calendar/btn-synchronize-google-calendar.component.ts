import { Component, Input } from '@angular/core';
import { GoogleAPIService } from '../../../../services/googleAPI.service';

@Component({
  selector: 'app-btn-synchronize-google-calendar',
  templateUrl: './btn-synchronize-google-calendar.component.html',
  styleUrls: [ './btn-synchronize-google-calendar.component.css' ]
})

export class BtnSynchronizeGoogleCalendarComponent {

  calendarIcon = 'assets/images/ico-google-calendar.png';
  btnText = 'Synchronize with Google Calendar';

  constructor(public googleApi: GoogleAPIService) {
  }
}
