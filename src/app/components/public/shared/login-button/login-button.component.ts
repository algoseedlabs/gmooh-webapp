import { Component, OnInit, Input, OnDestroy, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { ReservationService } from '../../../../services/reservation.service';
import { ApiService } from '../../../../services/api.service';
import { UserService } from '../../../../services/user/user.service';
import { environment } from '../../../../../environments/environment';
import { MatDialog } from '@angular/material';
import { LoginComponent } from '../../auth/login/login.component';
import { MODAL_WIDTH, MODAL_MOBILE_WIDTH } from '../../header/header.component';
import { AvatarService } from '../../../../services/user/avatar.service';
import { AuthService } from '../../../../services/auth.service';


@Component({
  selector: 'app-login-button',
  templateUrl: './login-button.component.html',
  styleUrls: ['./login-button.component.css']
})
export class LoginButtonComponent implements OnInit, OnDestroy {

  @Input() isMobile;
  @Input() blackText = false;

  reservationListSub;
  loginStateSub;
  notLoggedInImg = 'assets/images/login.png';
  menuIsOpen: boolean;
  reservationListIsEmpty: boolean;
  loggedIn: boolean;
  shortUserName;
  baseUrl = environment.BASE_URL;

  constructor(
    private reservation: ReservationService,
    private router: Router,
    private apiService: ApiService,
    private user: UserService,
    public avatar: AvatarService,
    public dialog: MatDialog,
    private auth: AuthService
  ) { }

  @HostListener('document:click', ['$event'])
  onDocumentClick(e): void {
    this.menuIsOpen = !!e.target.closest('.logged-in-btn');
  }

  ngOnInit() {

    this.reservationListSub = this.reservation.reservationListIsEmpty.subscribe(res => {
      this.reservationListIsEmpty = res;
    });

    this.loginStateSub = this.user.loggedInState.subscribe(res => {
      this.loggedIn = res;
      if (res) {
        this.setAvatarAndName();
      }
    });

  }

  ngOnDestroy() {
    this.reservationListSub.unsubscribe();
    this.loginStateSub.unsubscribe();
  }

  setAvatarAndName() {
    const { firstName } = this.user.userData;
    this.shortUserName = firstName.length > 10 ? firstName.slice(0, 10) + '...' : firstName;

    this.avatar.setAvatarUrl(this.user.userData);
  }

  logOut(e): void {
    this.closeMenu(e);
    this.user.clearUserData();
    this.auth.clearSocialAccountToken();
    this.auth.clearRegData();
    this.apiService.clearReservationData();
    localStorage.clear();
    this.user.loggedInState.next(false);
    this.reservationListIsEmpty = true;
    this.router.navigate([ '/main' ]);
  }

  openLogin(): void {
    this.dialog.open(LoginComponent, {
      width: this.isMobile ? MODAL_MOBILE_WIDTH : MODAL_WIDTH,
      data: {
        isMobile: this.isMobile
      }
    });
  }

  navigate(e, route) {
    this.closeMenu(e);
    this.router.navigate([ route ]);
  }

  closeMenu(e) {
    e.stopPropagation();
    this.menuIsOpen = false;
  }

  openMenu(): void {
    this.menuIsOpen = !this.menuIsOpen;
  }

}
