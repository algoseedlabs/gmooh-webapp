import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ShareService } from '@ngx-share/core';

@Component({
  selector: 'app-social-sharing',
  templateUrl: './social-sharing.component.html',
  styleUrls: ['./social-sharing.component.css']
})
export class SocialSharingComponent implements OnInit {

  @Input() shareLink;
  @Input() left = 40;
  @Output() press = new EventEmitter<any>();

  socialIcons = [
    {
      link: 'assets/images/social/linkedin.png',
      title: 'linkedin'
    },
    {
      link: 'assets/images/social/twitter.png',
      title: 'twitter'
    },
    {
      link: 'assets/images/social/facebook.png',
      title: 'facebook'
    },
    {
      link: 'assets/images/social/whatsapp.png',
      title: 'whatsapp'
    },

  ];

  constructor(
    public share: ShareService
  ) { }

  ngOnInit() {
  }

}
