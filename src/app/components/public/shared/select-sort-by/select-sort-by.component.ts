import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IItemOfListOfNamesToSort, IOptionsForSortByCityPlaces } from '../../../../interfaces/shared-components';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-select-sort-by',
  templateUrl: './select-sort-by.component.html',
  styleUrls: [ './select-sort-by.component.css' ]
})

export class SelectSortByComponent implements OnInit {

  @Input() listOfNamesToSort: IItemOfListOfNamesToSort[];
  @Input() max: number;
  @Input() cityId: number;
  @Input() cityPlace: any;
  @Input() queryParams;
  @Output() setData = new EventEmitter<any>();
  activeItem: IItemOfListOfNamesToSort;

  constructor(
    private apiService: ApiService
  ) {}

  ngOnInit() {
    this.activeItem = this.listOfNamesToSort.find(el => el.active);
    this.sortData(this.activeItem);
  }

  sortData(item: IItemOfListOfNamesToSort): void {
    this.activeItem = item;
    const data: IOptionsForSortByCityPlaces = this.getData();
    this.apiService.sortByCityPlaces(data).subscribe((res) => {
      if (res.status === 'SUCCESS') {
        if (res.response.length > 0) {
          this.setData.emit(res.response);
        }
      }
    });
  }

  getData(): IOptionsForSortByCityPlaces {
    const dateFrom = this.queryParams.departure;
    const dateTo = this.queryParams.arrival;
    return {
      cityId: this.cityId,
      type: this.cityPlace.id,
      sortBy: this.activeItem.id,
      nextPageToken: '',
      dateFrom,
      dateTo
    };
  }


}
