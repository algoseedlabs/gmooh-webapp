import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import {  MatDialogRef } from '@angular/material';



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: [ './registration.component.css' ],
  animations: [
    trigger('moveBorder', [
      state('email', style({
        left: 0,
      })),
      state('information', style({
        left: 'calc(100% / 3)',
      })),
      state('avatar', style({
        left: 'calc((100% / 3) * 2)',
      })),
      transition('* => *', [
        animate('0.2s')
      ])
    ])
  ],
})
export class RegistrationComponent implements OnInit {

  tabs = [ 'email', 'information', 'avatar' ];
  currentPage = this.tabs[0];
  activeColor = '#00919a';

  constructor(
    public dialogRef: MatDialogRef<any>,
  ) {}

  ngOnInit() {
  }

  setPage(pageName): void {
    this.currentPage = pageName;
  }

  setPageTab(page): void {
    const currentIndex = this.tabs.indexOf(this.currentPage);
    const targetIndex = this.tabs.indexOf(page);
    if (targetIndex < currentIndex) {
      this.currentPage = page;
    }
  }

  closeModal() {
    this.dialogRef.close();
  }

}
