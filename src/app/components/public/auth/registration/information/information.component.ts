import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService as Auth} from '../../../../../services/auth.service';
import { UserService } from '../../../../../services/user/user.service';


@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})

export class InformationComponent implements OnInit {

  nameRegex = new RegExp('^[A-Z][a-z]*$');
  phoneRegex = new RegExp('^[\\d]{6,20}$');
  nameErrorText = 'name must contain first capital letter A-Z and letters a-z without whitespaces.';
  submitClicked = false;

  form = new FormGroup({
    firstName: new FormControl('',
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern(this.nameRegex)
      ]),
    lastName: new FormControl('',
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20),
        Validators.pattern(this.nameRegex)
      ]),
    birthday: new FormControl('', [Validators.required]),
    gender: new FormControl(''),
    phoneNumber: new FormControl('',
      [
        Validators.pattern(this.phoneRegex),
      ]),
    country: new FormControl('', [Validators.maxLength(100)]),
  });

  @Output() nextClicked: EventEmitter<any> = new EventEmitter<any>();


  constructor(
    private auth: Auth,
    public user: UserService
  ) {}

  public handleAddressChange(address) {
    this.form.controls.country.setValue(address.formatted_address);
    this.setRegData();
  }

  ngOnInit() {
     if (this.auth.regData.firstName) {
       this.form.patchValue(this.auth.regData);
     }
     this.setGender();
  }

  setGender() {
    this.form.controls.gender.setValue(this.auth.regData.gender || this.user.genders[0]);
  }

  goToAvatar(): void {
    this.submitClicked = true;
    if (!this.form.invalid) {
      this.nextClicked.emit('avatar');
    }
  }

  setRegData(): void {
    this.auth.regData = this.form.value;
  }
}

