import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import * as moment from 'moment';
import { MatDialog, MatDialogRef } from '@angular/material';
import { UserService } from 'src/app/services/user/user.service';
import { AuthService as Auth } from '../../../../../services/auth.service';


@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: [ './avatar.component.css' ]
})
export class AvatarComponent implements OnInit {

  userData;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<any>,
    private _apiService: ApiService,
    private userService: UserService,
    public auth: Auth
  ) {}


  ngOnInit() {
  }

  onChooseAvatar(event: Event): void {
    this.userService.onChooseAvatar(event, result => {
      this.auth.avatar = result;
    });
  }

  onSubmitSave(event) {
    event.preventDefault();
    if (this.auth.avatar) {
      this.auth.registrationError = '';
      this.handleRegistrationEnd();
    } else {
      this.auth.registrationError = 'Choose avatar or skip';
    }
  }

  onSubmitSkip(event) {
    event.preventDefault();
    this.auth.avatar = null;
    this.handleRegistrationEnd();
  }

  handleRegistrationEnd() {
    this.prepareDataForSave();
    this._apiService.sendRegData(this.userData).subscribe(data => {
      if (data.status === 'SUCCESS') {
        this.dialogRef.close();
        this.auth.avatar ? this.auth.openSuccessModal() : this.auth.openMoreInfoModal();
        this.auth.clearRegData();
        this.userService.userData = data.response;
        this.userService.loggedInState.next(true);
      } else {
        this.auth.registrationError = data.errorMessage;
      }
    });
  }

  prepareDataForSave() {
    this.userData = this.userService.prepareDataForSave(this.auth.regData);
    if (this.auth.avatar) {
      this.userData.avatar = this.auth.avatar.split('base64,')[1];
    }
    this.userData.birthday = moment(this.userData.birthday).format('MM/DD/YYYY');
  }

}

