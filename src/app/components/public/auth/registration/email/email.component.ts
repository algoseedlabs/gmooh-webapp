import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService as Auth } from '../../../../../services/auth.service';



@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {

  submitClicked = false;

  public regexp = new RegExp ( "^(([^<>()[\\]\\\\.,;:\\s@\\\"]+(\\.[^<>()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
  public strongRegex = new RegExp ("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");

  @Output() nextClicked: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private auth: Auth
  ) {}

  form = new FormGroup({
    email: new FormControl('',
      [
        Validators.required,
        Validators.pattern(this.regexp)
      ]),
    password: new FormControl('',
      [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20),
        Validators.pattern(this.strongRegex)
      ]),
    receiveMsg: new FormControl(true),
    agreement: new FormControl(true, [Validators.pattern('true')]),

  });

   ngOnInit() {
     if (Object.keys(this.auth.regData).length > 0) {
      this.form.patchValue(this.auth.regData);
    }
   }


  goToInformation() {
    this.submitClicked = true;
    if (!this.form.invalid) {
      this.nextClicked.emit('information');
    }
  }

  setRegData() {
    this.auth.regData = this.form.value;
  }
}
