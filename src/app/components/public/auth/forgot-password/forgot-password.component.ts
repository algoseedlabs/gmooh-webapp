import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {


  emailForm = new FormGroup({
    email: new FormControl('')
  });

  constructor(
    private api: ApiService,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data,
    public dialog: MatDialog,
    private auth: AuthService
  ) { }

  ngOnInit() {
  }

  sendEmail() {
    this.auth.sendForgotPassRequest(this.emailForm.value);
    this.closeModal();
  }

  closeModal() {
    this.dialogRef.close();
  }
}
