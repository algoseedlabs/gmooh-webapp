import { Component, Inject, OnInit } from '@angular/core';
import { RegistrationComponent } from '../registration/registration.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MODAL_WIDTH, MODAL_MOBILE_WIDTH } from '../../header/header.component';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { UserService } from '../../../../services/user/user.service';
import { AuthService } from '../../../../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit {

  isMobile = false;
  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<any>,
    private apiService: ApiService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data,
    private userService: UserService,
    public auth: AuthService
  ) {
  }

  ngOnInit() {
    this.isMobile = this.data.isMobile;
  }

  openSignup() {
    this.closeModal();
    this.dialog.open(RegistrationComponent, {
      width: this.isMobile ? MODAL_MOBILE_WIDTH : MODAL_WIDTH,
      data: {
        isMobile: this.isMobile
      }
    });
  }

  openForgotPasswordModal() {
    this.closeModal();
    this.dialog.open(ForgotPasswordComponent, {
      width: this.isMobile ? MODAL_MOBILE_WIDTH : MODAL_WIDTH,
    });
  }

  onSubmit() {
    this.apiService.login(this.loginForm.value).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.closeModal();
        this.auth.loginError = undefined;
        this.auth.openSuccessModal();
        this.userService.userData = res.response;
        this.userService.loggedInState.next(true);
      } else {
        if (res.errorMessage === 'ERROR_WRONG_AUTH_DATA') {
          this.auth.loginError = 'Wrong data';
        } else {
          this.auth.loginError = res.errorMessage;
        }
      }
    });
  }

  closeModal() {
    this.dialogRef.close();
  }

}
