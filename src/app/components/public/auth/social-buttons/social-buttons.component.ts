import {Component, Inject, OnInit} from '@angular/core';
import {AuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {IDataGoogle} from '../../../../interfaces/data-google';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {ApiService} from '../../../../services/api.service';
import {Router} from '@angular/router';
import {UserService} from '../../../../services/user/user.service';
import { AuthService as Auth } from '../../../../services/auth.service';

@Component({
  selector: 'app-social-buttons',
  templateUrl: './social-buttons.component.html',
  styleUrls: ['./social-buttons.component.css']
})
export class SocialButtonsComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<any>,
    private api: ApiService,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data,
    private userService: UserService,
    private authService: AuthService,
    private auth: Auth
  ) { }

  ngOnInit() {
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(res => {
      console.log('resp Fb', res);
      this.sendRegRequest(res);
    });

  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res: SocialUser) => {
      console.log('res', res)
      const accountName: string = res.provider.toLocaleLowerCase();
      this.auth.socialAccountToken = { [accountName]: res.authToken };
      this.sendRegRequest(res);
      const dataGoogle: IDataGoogle = {
        authToken: res.authToken,
        email: res.email
      };
      this.api.setDataGoogle(dataGoogle);
    }).catch(err => console.log(err));
  }

  sendRegRequest(regData) {
    this.api.loginWithSocial(regData).subscribe(res => {
      if (res.status === 'SUCCESS') {
        this.dialogRef.close();
        this.userService.userData = res.response;
        this.userService.userData = { socialProvider: regData.provider };
        this.userService.loggedInState.next(true);
        this.auth.openMoreInfoModal();
      } else {
        if (res.errorMessage === 'ERROR_WRONG_AUTH_DATA') {
          this.auth.loginError = 'Wrong data';
        } else {
          this.auth.loginError = res.errorMessage;
        }
      }
    });
  }


}
