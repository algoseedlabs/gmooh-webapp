import { Component, Inject, OnInit } from '@angular/core';
import { DirectivesService } from '../../../../services/directives.service';
import { interval } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-profile-more-info',
  templateUrl: './profile-more-info.component.html',
  styleUrls: ['./profile-more-info.component.css'],
})
export class ProfileMoreInfoComponent implements OnInit {

  baseUrl = environment.BASE_URL;
  avatarSrc;
  avatarPlaceholder = 'assets/images/default-avatar.png';
  plane = 'assets/images/plane.png';
  leftPlane = 'assets/images/plane-left.png';

  mobileTraces = [
    {left: 30, top: 17},
    {left: 38, top: 15},
    {left: 47, top: 14},
    {left: 57, top: 14},
    {left: 67, top: 15},
    {left: 75, top: 17},
    {left: 82, top: 20},
    {left: 88, top: 24},
    {left: 93, top: 29},
  ];
  rightTraces = [];
  leftTraces = [];

  constructor(
    private directivesService: DirectivesService,
    @Inject(MAT_DIALOG_DATA) public data,
    private router: Router,
    public dialogRef: MatDialogRef<any>,
    private user: UserService,
  ) { }

  ngOnInit() {
    const { avatar } = this.user.userData;
    if (avatar) {
      this.avatarSrc =  avatar.includes('http') ? avatar : this.baseUrl + avatar;
    }

    const tick = interval(50);
    let count = 0;
    const tickSubscription = tick.subscribe(n => {
      count += 1;
      const rightCoords = this.directivesService.rightTrace;
      const leftTraces = this.directivesService.leftTrace;
      this.rightTraces = [...rightCoords];
      this.leftTraces = [...leftTraces];
      if (count === 30) {
        tickSubscription.unsubscribe();
      }
    });
  }

  goToProfile() {
    this.dialogRef.close();
    this.router.navigate(['/account/profile']);
  }

  closeModal() {
    this.dialogRef.close();
  }

}
