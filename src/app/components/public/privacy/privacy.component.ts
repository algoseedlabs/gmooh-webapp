import { Component, OnInit } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: [ './privacy.component.css' ]
})
export class PrivacyComponent implements OnInit {
  isMobile: boolean;

  constructor(private deviceDetectorService: DeviceDetectorService) {
  }

  ngOnInit() {
    this.isMobile = this.deviceDetectorService.isMobile();
  }

}
