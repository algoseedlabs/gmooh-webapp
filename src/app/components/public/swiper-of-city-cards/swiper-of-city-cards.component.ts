import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { ApiService } from '../../../services/api.service';
import { MapService } from '../../../services/map.service';
import { RouteService } from '../../../services/route.service';
import { Router } from '@angular/router';
import { DirectivesService } from '../../../services/directives.service';
import { ISection } from '../../../interfaces/cities';
import { IPlaceInfo } from '../../../interfaces/places';
import { EventPageService } from '../../../services/event-page.service';
import { SECTIONS } from '../../../app-constants';
import { SwiperService } from '../../../services/swiper.service';
import { SwiperDirective } from 'ngx-swiper-wrapper';
import { ReservationService } from 'src/app/services/reservation.service';
import { WindowService } from 'src/app/services/window.service';


@Component({
  selector: 'app-swiper-of-city-cards',
  templateUrl: './swiper-of-city-cards.component.html',
  styleUrls: ['./swiper-of-city-cards.component.css']
})


export class SwiperOfCityCardsComponent implements OnInit {

  private iconsSrc = {
    accomondation: 'assets/images/hotel.png',
    shop: 'assets/images/shopping-bag.png',
    restaurant: 'assets/images/silverware.png',
    events: 'assets/images/ticket.png',
    landnarks: 'assets/images/landnarks.png'
  }


  @Input() data;
  @Input() cityPlaceData;
  @Input() cityId;
  @Input() index;
  @Input() section: string;
  nextPageToken: string;
  pageNumber: number;
  loading: boolean;

  defaultImage = 'assets/images/no-image.png';

  activeIndex = 0;

  sections: ISection[] = SECTIONS;
  baseUrl = environment.BASE_URL;
  markerAdded = false;

  @ViewChild(SwiperDirective) directiveRef?: SwiperDirective;

  constructor(
    private apiService: ApiService,
    private mapService: MapService,
    private eventPageService: EventPageService,
    private reservationService: ReservationService,
    private windowService: WindowService,
    private swiperService: SwiperService
  ) {
    swiperService.setConfig({ spaceBetween: 20 });
  }

  ngOnInit() {
  }

  onMouseOver(lat, lng) {
    if (!this.markerAdded) {
      this.mapService.setCurrentCoords('marker', lat, lng);
      this.markerAdded = true;
    }
  }

  onMouseleave(): void {
    this.mapService.showMarker.next(false);
    this.markerAdded = false;
  }

  onEventClick(id: string, data: IPlaceInfo[]): void {
    this.eventPageService.goToEvent(id, data, this.section, this.cityId);
  }

  setActiveIndex(index: number): void {
    this.activeIndex = index;
  }

  onChange() {
    if (!this.loading) {
      if (this.section === this.sections[3].title) {
        // uncomment when the backend is fixed
        // this.loading = true;
        // if (!this.cityPlaceData.last) {
        //   const cityName: string = this.apiService.userClickedData.cityName;
        //   const flightsData = this.apiService.flightsData;
        //   const dateFrom = this.parseDate(flightsData.departure);
        //   const dateTo = this.parseDate(flightsData.arrival);
        //   const data = {
        //     cityName,
        //     dateFrom,
        //     dateTo,
        //     pageNumber: String(++this.pageNumber)
        //   };
        //   this.apiService.getMoreEvents(data).subscribe(res => {
        //     this.changeData(this.activeIndex, res);
        //   });
        //
        // }

      } else {
        if (this.nextPageToken) {
          this.loading = true;
          const activeSection = this.sections.find((section: ISection) => {
            return section.title.toLocaleLowerCase() === this.section.toLocaleLowerCase();
          });
          const data = {
            cityInfoId: this.cityId,
            itemForSearch: activeSection.id,
            nextPageToken: this.nextPageToken
          };
          this.apiService.getMorePlaces(data).subscribe(res => {
            this.changeData(this.activeIndex, res);
          });

        }
      }
    }
  }

  parseDate(date: string): string {
    const newDate = new Date(date);
    const options = { month: '2-digit', day: '2-digit', year: 'numeric' };
    return newDate.toLocaleDateString('en-US', options);
  }

  changeData(index: number, res: any): void {
    if (res.status === 'SUCCESS' &&
      res.response &&
      res.response.items &&
      res.response.items.length) {
      this.data.push(...res.response.items);
      this.directiveRef.setIndex(index);
      this.nextPageToken = res.response.nextPageToken ? res.response.nextPageToken : null;
      // server side error for events
      this.pageNumber = res.response.pageNumber ? res.response.pageNumber : null;
      this.cityPlaceData.last = res.response.last ? res.response.last : null;
    } else {
      console.log(res.errorMessage);
    }
    this.loading = false;
  }

  trackEventId(index: number, event: IPlaceInfo): string {
    return event ? event.placeId : undefined;
  }

  getImage(city, i) {
    if (city.type === 5 && city.image) {
        return city.image;
    }

    if (city.images && city.images[i]) {
        return this.baseUrl + city.images[i];
    } else if (city.images && i) {
        return '';
    }

    return this.defaultImage;
  }

  getIconByTypePlace(type: number) {
    switch (type) {
      case 1:
        return this.iconsSrc.accomondation;
      case 2:
        return this.iconsSrc.restaurant;
      case 3:
        return this.iconsSrc.shop;
      case 4:
        return this.iconsSrc.landnarks;
      case 5:
        return this.iconsSrc.events;
      default:
        return '';
    }
  }

  bookReservation(index: number): void {
    const isMobile = !this.windowService.isDesktop;
    this.reservationService.eventReservation(this.data[index], isMobile);
  }
}
