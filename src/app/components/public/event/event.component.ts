import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { URLS } from '../../../api-urls';
import { environment } from '../../../../environments/environment';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { MapService } from '../../../services/map.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { MatDialog } from '@angular/material';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MODAL_MOBILE_WIDTH, MODAL_WIDTH } from '../header/header.component';
import { ErrorComponent } from '../@dialogs/error/error.component';
import { IPlaceData } from '../../../interfaces/places';
import { ISection } from '../../../interfaces/cities';
import { ReservationService } from '../../../services/reservation.service';
import { SECTIONS } from '../../../app-constants';
import { WindowService } from 'src/app/services/window.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css'],
  animations: [
    trigger('slideDown', [
      state('narrow', style({
        width: '80%'
      })),
      state('wide', style({
        width: '100%'
      })),
      transition('* => *', [animate('0.3s')])
    ]),
    trigger('heightType', [
      state('high', style({
        height: 'calc(100vh - FOOTER_HEIGHT)'
      })),
      state('small', style({
        height: 'calc((100vh - FOOTER_HEIGHT) / 2)'
      })),
      transition('high <=> small', [animate('0.1s')])
    ])
  ]
})
export class EventComponent implements OnInit {
  sections: ISection[] = SECTIONS;
  baseUrl = environment.BASE_URL;
  eventRequestUrl;
  eventData: IPlaceData;
  swipeData = [];
  openHours;
  isEvent;
  isMobile: boolean;
  isDesktop: boolean;
  isSearching = true;
  eventType;
  queryParams;

  @ViewChild('eventPage') eventPage: ElementRef<HTMLElement>;

  constructor(
    private apiService: ApiService,
    private location: Location,
    private router: Router,
    private route: ActivatedRoute,
    private mapService: MapService,
    private deviceDetectorService: DeviceDetectorService,
    public dialog: MatDialog,
    private reservationService: ReservationService,
    private windowService: WindowService
  ) {
  }


  ngOnInit() {
    this.isMobile = this.deviceDetectorService.isMobile();
    this.isDesktop = this.deviceDetectorService.isDesktop();
    this.getEvent();
  }


  getEvent() {
    this.route.queryParams.subscribe(params => {
      this.isSearching = true;
      this.queryParams = params;
      this.isEvent = +params.isEvent;
      this.eventRequestUrl = this.isEvent ? URLS.cityEvents : URLS.cityPlaces(+params.cityId);

      const data = this.apiService.userClickedData.chosenCityItem;
      this.swipeData = [...data].filter(item => item.placeId !== params.placeId);

      if (params.placeId) {
        this.apiService.getEvents(this.eventRequestUrl, params.placeId).subscribe(
          res => {
            if (res.status === 'SUCCESS') {
              this.isSearching = false;
              const _data = res.response;
              if (this.isEvent) {
                const eventData = _data.eventInfoItem;
                this.eventType = this.sections.find(el => el.id === eventData.type).title;
                this.eventData = eventData;
                this.mapService.setCurrentCoords('location', eventData.latitude, eventData.longitude);
                setTimeout(() => {
                  this.mapService.setCurrentCoords('marker', eventData.latitude, eventData.longitude);
                }, 1000);
              } else {
                this.eventType = this.sections.find(el => el.id === _data.item.type).title;
                this.eventData = _data.item;
                if (this.eventData.openingHours) {
                  const hoursOfWork = this.eventData.openingHours.weekDayHours[this.getDayOfWeek()].split(': ')[1];
                  if (hoursOfWork === 'Open 24 hours') {
                    this.openHours = hoursOfWork;
                  } else {
                    this.openHours = 'Opens ' + hoursOfWork.split('–')[0];
                  }
                }
                this.mapService.setCurrentCoords('location', this.eventData.latitude, this.eventData.longitude);
                setTimeout(() => {
                  this.mapService.setCurrentCoords('marker', this.eventData.latitude, this.eventData.longitude);
                }, 1000);
              }

              this.scrollToFirstElement();
            } else {
              this.handleError();
            }
          },
          err => {
            this.handleError();
          }
        );
      }
    });

  }

  handleError() {
    const error = this.dialog.open(ErrorComponent, {
      width: this.isMobile ? MODAL_MOBILE_WIDTH : MODAL_WIDTH,
      data: {
        isMobile: this.isMobile,
        text: 'Data not found'
      }
    });
    error.afterClosed().subscribe(res => {
      this.location.back();
    });
  }

  getDayOfWeek(): number {
    let dayNumber = new Date().getDay();
    dayNumber = dayNumber === 0 ? 6 : dayNumber - 1;
    return dayNumber;
  }

  bookReservation(): void {
    this.reservationService.eventReservation(this.eventData, this.isMobile);
  }

  private scrollToFirstElement() {
    if (!this.windowService.isDesktop) {
      const scrollTo = this.eventPage.nativeElement.offsetTop + window.innerHeight;
      window.scrollTo(0, scrollTo);
    }
  }
}
