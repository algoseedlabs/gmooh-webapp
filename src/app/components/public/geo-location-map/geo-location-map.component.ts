import { Component, OnInit } from '@angular/core';
import { MapService } from '../../../services/map.service';
import { RouteService } from '../../../services/route.service';

@Component({
  selector: 'app-geo-location-map',
  templateUrl: './geo-location-map.component.html',
  styleUrls: ['./geo-location-map.component.css']
})
export class GeoLocationMapComponent implements OnInit {

  constructor(
    private routeService: RouteService,
    private mapService: MapService,
    ) { }
  public markers = [];
  public geoLocation;
  ngOnInit() {
    this.mapService.onMapGotCoords.subscribe(showMarker => {
      this.getCoords(showMarker);
    });

  }

  getCoords(bool) {
    this.mapService.getCurrentCoords().subscribe(
      res => {
        this.geoLocation = res.location;
        if (bool) {
          this.markers.push({...res.marker, label: 'A'});
        } else if (this.routeService.isEventPage) {
          this.markers.pop();
        } else {
          this.markers = [];
        }
      }
    );
  }




}


