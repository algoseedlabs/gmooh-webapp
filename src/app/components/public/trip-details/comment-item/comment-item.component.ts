import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { environment } from 'src/environments/environment';
import { AuthService } from '../../../../services/auth.service';



@Component({
  selector: 'app-comment-item',
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.css']
})
export class CommentItemComponent implements OnInit {

  @Input() review;
  @Input() commentAvatar;
  @Input() user;
  @Output() fingerClick: EventEmitter<any> = new EventEmitter<any>();

  isMobile = false;
  baseUrl = environment.BASE_URL;

  constructor(
    private api: ApiService,
    private auth: AuthService
  ) { }

  ngOnInit() {
  }

  likeComment(commentId): void {
    if (this.user.token) {
      this.api.likeComment(commentId, this.user.token).subscribe(res => {
        if (res.status === 'SUCCESS') {
          this.fingerClick.emit();
        }
      });
    } else {
      this.auth.openLoginModal();
    }
  }

  dislikeComment(commentId): void {
    if (this.user.token) {
      this.api.dislikeComment(commentId, this.user.token).subscribe(res => {
        if (res.status === 'SUCCESS') {
          this.fingerClick.emit();
        }
      });
    } else {
      this.auth.openLoginModal();
    }
  }

}
