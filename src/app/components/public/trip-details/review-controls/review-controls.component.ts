import { Component, OnInit, Input, EventEmitter, Output, HostListener } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiService } from '../../../../services/api.service';
import { Router } from '@angular/router';
import { UserService } from '../../../../services/user/user.service';
import { environment } from '../../../../../environments/environment';
import { ShareService } from '@ngx-share/core';


@Component({
  selector: 'app-review-controls',
  templateUrl: './review-controls.component.html',
  styleUrls: [ '../trip-details.component.css', './review-controls.component.css']
})
export class ReviewControlsComponent implements OnInit {




  @Input() tripDetails;
  @Input() tripId;
  @Input() tripImage;
  @Output() onLikeTrip = new EventEmitter<any>();

  shareMenuIsVisible = false;

  likeIcon = 'assets/images/like-review.png';
  activeLikeIcon = 'assets/images/active-review-like.png';
  copyIcon = 'assets/images/copyIcon.png';
  shareIcon = 'assets/images/share-review.png';
  shareLink;

  constructor(
    private _snackBar: MatSnackBar,
    private api: ApiService,
    private user: UserService,
    private router: Router,

  ) { }

  @HostListener('window:click', ['$event.target'])
  onClick(e) {
    if (!e.closest('.share-block') && this.shareMenuIsVisible) {
      this.shareMenuIsVisible = false;
    }
  }

  ngOnInit() {
    this.shareLink = 'https://gmooh.app/trip-details/' + this.tripId;

  }

  likeTrip() {
    const { token } = this.user.userData;
    if (token) {
      this.api.likeTrip(this.tripId, token).subscribe(data => {
        if (data.status === 'SUCCESS') {
          this.onLikeTrip.emit();
        }
      });
    }
  }


  copyLink() {
    const shareLink = window.location.href;
    const buffer: any = navigator;
    buffer.clipboard.writeText(shareLink);
    this._snackBar.open('Link copied', '', {
      duration: 1000,
    });
  }

  copyTrip() {
    this.router.navigate([`/booking-details/copy-trip/${this.tripId}`]);
  }

  openShareMenu() {
    this.shareMenuIsVisible = !this.shareMenuIsVisible;
  }

}
