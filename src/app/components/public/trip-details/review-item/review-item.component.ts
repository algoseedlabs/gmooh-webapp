import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import { environment } from 'src/environments/environment';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-review-item',
  templateUrl: './review-item.component.html',
  styleUrls: ['./review-item.component.css']
})
export class ReviewItemComponent {

  @Input() review;
  @Input() index;
  @Input() commentAvatar;
  @Input() user;
  @Output() fingerClick: EventEmitter<any> = new EventEmitter<any>();
  isMobile = false;
  baseUrl = environment.BASE_URL;

  constructor(
    private api: ApiService,
    private auth: AuthService
  ) { }

  likeReview(reviewId): void {
    if (this.user.token) {
      this.api.likeReview(reviewId, this.user.token).subscribe(res => {
        if (res.status === 'SUCCESS') {
          this.emitEvent();
        }
      });

    } else {
      this.auth.openLoginModal();
    }
  }

  dislikeReview(reviewId): void {
    if (this.user.token) {
      this.api.dislikeReview(reviewId, this.user.token).subscribe(res => {
        if (res.status === 'SUCCESS') {
          this.emitEvent();
        }
      });
    } else {
      this.auth.openLoginModal();
    }
  }

  emitEvent(): void {
    this.fingerClick.emit();
  }


}
