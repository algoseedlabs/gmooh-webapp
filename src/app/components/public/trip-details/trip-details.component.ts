import {Component, OnInit, Inject, OnDestroy} from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ApiService } from 'src/app/services/api.service';
import { UserService } from 'src/app/services/user/user.service';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-trip-details',
  templateUrl: './trip-details.component.html',
  styleUrls: [ './trip-details.component.css' ]
})
export class TripDetailsComponent implements OnInit, OnDestroy {

  isMobile: boolean;
  baseUrl = environment.BASE_URL;
  tripDetails;
  currentTripId;
  tripReviewState = false;
  review;
  commentAvatar = 'assets/images/avatar.png';

  user;

  constructor(
    private deviceDetectorService: DeviceDetectorService,
    private apiService: ApiService,
    private userService: UserService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.user = this.userService.userData;
    this.currentTripId = this.route.snapshot.paramMap.get('id');
    this.isMobile = this.deviceDetectorService.isMobile();

    this.getTrip();
    this.getReview();
  }

  ngOnDestroy() {
    this.editMetaTags({
      image: 'https://gmooh.app/assets/images/share-logo.png',
      title: 'Gmooh',
      url: 'https://gmooh.app/'
    });
  }

  getTrip() {
    this.apiService.getSingleTrip(this.currentTripId, this.user.token).subscribe(data => {
      console.log(data.response);
      this.tripDetails = data.response;
      this.tripDetails.images = this.tripDetails.images.splice(0, 3);

      this.editMetaTags({
        image: environment.BASE_URL + this.tripDetails.images[0],
        title: 'Gmooh: my perfect trip to ' + this.tripDetails.destinationCity,
        url: 'https://gmooh.app/trip-details/' + this.currentTripId
      });

      const days = [];
      data.response.detailsList.forEach((el, index) => {
        const date = el.dateFrom.split(' ');
        el['time'] = date[1].slice(0, 5);
        if (index === 0) {
          days.push({
            date: date[0],
            events: [ el ]
          });
        } else {
          let dateExists = false;
          let dateIndex = 0;
          days.forEach((item, key) => {
            if (item.date === date[0]) {
              dateExists = true;
              dateIndex = key;
            }
          });
          if (dateExists) {
            days[dateIndex].events.push(el);
          } else {
            days.push({
              date: date[0],
              events: [ el ]
            });
          }
        }

      });
      this.tripDetails['detailsList'] = days;
    });
  }

  getReview() {
    this.apiService.getTripReviews(this.currentTripId, this.user.token).subscribe(data => {
      if (data.status === 'SUCCESS') {
        this.tripReviewState = true;
        this.review = data.response;
      }
    });
  }


  editMetaTags(data) {
    if (window.location.hostname === 'gmooh.app') {
      this.apiService.editMetaTags(data).subscribe(res => {});
    }
  }

}
