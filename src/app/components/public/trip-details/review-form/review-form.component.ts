import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ApiService } from '../../../../services/api.service';
import { UserService } from '../../../../services/user/user.service';
import { environment } from '../../../../../environments/environment';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.css', '../trip-details.component.css']
})
export class ReviewFormComponent implements OnInit {

  baseUrl = environment.BASE_URL;
  rating;
  user = this.userService.userData;
  commentAvatar = 'assets/images/avatar.png';
  @Input() tripReviewState;
  @Input() tripId;
  @Input() review;
  @Output() onSendReview = new EventEmitter<any>();
  @Output() onAddReview = new EventEmitter<any>();

  constructor(
    private api: ApiService,
    private userService: UserService,
    private auth: AuthService
  ) { }

  commentForm = new FormGroup({
    comment: new FormControl('')
  });

  ngOnInit() {
  }

  addReview() {
    const { token } = this.user;
    const review = {
      mark: this.rating,
      text: this.commentForm.value.comment,
      tripId: this.tripId,
    };
    this.onAddReview.emit();

    if (token) {
      this.api.sendTripReview(review, token).subscribe(res => {
        this.clearComment();
        this.onSendReview.emit();
      });
    } else {
      this.auth.openLoginModal();
    }
  }

  addComment() {
    const { token, id } = this.user;
    const comment = {
      mark: this.rating,
      reviewId: this.review.id,
      text: this.commentForm.value.comment,
      userId: id,
    };

    if (token) {
      this.api.sendCommentReview(comment, token).subscribe(res => {
        this.onSendReview.emit();
        this.clearComment();
      });
    } else {
      this.auth.openLoginModal();
    }
  }

  clearComment() {
    this.commentForm.controls.comment.setValue('');
    this.rating = 0;
  }

  onRatingClick(data: any) {
    this.rating = data.rating;
  }

}
