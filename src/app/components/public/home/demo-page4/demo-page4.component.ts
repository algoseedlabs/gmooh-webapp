import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-demo-page4',
  templateUrl: './demo-page4.component.html',
  styleUrls: ['./demo-page4.component.css'],
})
export class DemoPage4Component {

  @Input() isMobile;

  constructor() { }
}
