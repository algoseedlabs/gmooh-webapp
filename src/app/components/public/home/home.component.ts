import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {WindowService} from '../../../services/window.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],

})
export class HomeComponent implements OnInit, OnDestroy {

    isMobile: boolean;
    showDemo = false;
    windowSub;

    scrollConfig = {
        maxValue: 4,
        value: 0,
        translate: [100, 100, 100, 100]
    };
    scrollThrottle;


    constructor(
        private windowService: WindowService
    ) {
    }

    @HostListener('window:wheel', ['$event'])
    onHandleScroll(event): void {

        if (this.showDemo) {
            clearTimeout(this.scrollThrottle);
            this.scrollThrottle = setTimeout(_ => {
                if (event.deltaY > 0 && this.scrollConfig.value < this.scrollConfig.maxValue) {
                    this.scrollConfig.value += 1;
                }
                if (event.deltaY < 0 && this.scrollConfig.value > 0) {
                    this.scrollConfig.value -= 1;
                }
                this.handleSectionTranslate(this.scrollConfig.value);
            }, 100);
        }
    }

    handleSectionTranslate(value) {
        this.scrollConfig.translate = this.scrollConfig.translate.map((item, index) => item = value > index ? 0 : 100);
    }

    onNextPageClick(val) {
        this.scrollConfig.value += val;
        this.handleSectionTranslate(this.scrollConfig.value);
        if (!this.scrollConfig.value) {
            this.showDemo = false;
        }
    }

    onCancelDemo(): void {
        this.showDemo = false;
        this.scrollConfig = {
            maxValue: 4,
            value: 0,
            translate: [100, 100, 100, 100]
        };
    }

    ngOnInit() {
        this.windowSub = this.windowService.windowSize.subscribe(res => {
            this.isMobile = res.width < this.windowService.DESKTOP_WIDTH;
        });

    }

    ngOnDestroy() {
        this.windowSub.unsubscribe();
    }

    openDemo() {
        this.showDemo = true;
        setTimeout(_ => {
            this.scrollConfig.translate[0] = 0;
            this.scrollConfig.value = 1;
        }, 100);
    }
}










