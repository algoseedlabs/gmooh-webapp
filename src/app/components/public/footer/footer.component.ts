import { Component, OnInit } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  isMobile = false;

  constructor(private deviceDetectorService: DeviceDetectorService,) { }

  ngOnInit() {
    this.isMobile = this.deviceDetectorService.isMobile();
  }


}
