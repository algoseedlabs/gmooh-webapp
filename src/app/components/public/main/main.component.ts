import { Component, OnInit, OnDestroy } from '@angular/core';
import { WindowService } from '../../../services/window.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {


  isDesktop: boolean;
  isWindHeightSmall: boolean;
  windowSub;

  constructor(
    private windowService: WindowService,
    private location: Location
  ) {

  }

  ngOnInit() {
    this.windowSub = this.windowService.windowSize.subscribe(res => {
      this.isDesktop = res.width >= this.windowService.DESKTOP_WIDTH;
      this.isWindHeightSmall = res.height < 650 && this.isDesktop;
    });

  }

  ngOnDestroy() {
    this.windowSub.unsubscribe();
  }

  goBack(): void {
    this.location.back();
  }
}
