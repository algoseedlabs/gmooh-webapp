import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { WindowService } from '../../../services/window.service';

export const MODAL_WIDTH = '25%';
export const MODAL_MOBILE_WIDTH = '95vw';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.css' ],
})
export class HeaderComponent implements OnInit, OnDestroy {

  windowSub;
  scrollSub;
  headerHeight = 350;
  isWindHeightSmall: boolean;
  isMobile: boolean;
  headerIsVisible;


  constructor(
    private window: WindowService,
    private router: Router,
    private location: Location,
  ) {}

  ngOnInit() {

    this.windowSub = this.window.windowSize.subscribe(res => {
      this.isMobile = res.width < this.window.DESKTOP_WIDTH;
      this.isWindHeightSmall = res.height < 650 && !this.isMobile;
    });


    this.scrollSub = this.window.pageOffsetY.subscribe(data => {
      this.headerIsVisible = data.offsetY < this.headerHeight;
    });

  }

  ngOnDestroy() {
    this.windowSub.unsubscribe();
    this.scrollSub.unsubscribe();
  }


  goToMainPage(): void {
    this.router.navigate([ '' ]);
  }

  goBack(): void {
    this.location.back();
  }

}
