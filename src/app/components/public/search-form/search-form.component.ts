import { Component, OnInit, HostListener, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../services/api.service';
import * as moment from 'moment';
import { SearchService } from '../../../services/search.service';
import { SearchChipsService } from '../../../services/search-chips.service';
import { SECTIONS } from '../../../../app/app-constants';


@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: [ './search-form.component.css' ],
})

@HostListener('window:keyup', [ '$event' ])

export class SearchFormComponent implements OnInit {

  private citiesChips = [];

  @Input() isInteractive: boolean;
  @Input() showChips: boolean;

  minReturnDate = new Date();
  submitButtonClicked: boolean;
  locationPicked: boolean;
  delayTimer;
  dateValue = 'Choose date range';



  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    public search: SearchService,
    private chipsService: SearchChipsService
  ) {}

  ngOnInit() {
    const cashedFormData = { ...this.search.flightsData };
    if (Object.keys(cashedFormData).length) {
      this.search.form.patchValue(cashedFormData);
      this.setDatePlaceholder(cashedFormData);
      this.search.validateForm();
    }
    this.initCitiesChips();
  }

  setDatePlaceholder(data) {
    this.dateValue = data.departure + ' - ' + data.arrival;
  }

  handleLocationSearch(data: string, locationType: string): void {

    if (!this.isInteractive) {
      return;
    }

    if (data) {
      this.searchLocation(data);
    } else {
      this.search.setLocations([]);
      locationType === 'originCityName' ? this.search.clearOriginData() : this.search.clearDestinationData();
    }
  }

  searchLocation(value: string): void {
    this.api.searchLocation(value).subscribe(
      res => {
        if (res.status === 'SUCCESS' && res.response) {
          this.search.setLocations(res.response.slice(0, 5));
        }
      },
      err => console.log(err)
    );
  }

  onSelectOption(type: string, data) {
    this.locationPicked = true;
    this.handleSelection(type, data);
  }

  handleOnBlur(type: string): void {
    setTimeout(_ => {
      if (!this.locationPicked && this.search.locations.length > 0) {
        this.locationPicked = true;
        this.handleSelection(type, this.search.locations[0]);
        this.search.validateForm();
      }
    }, 100);
  }

  handleSelection(type, data) {
    const targetId = this.getTargetInputId(type);
    const newData = {
      [type]: data.placeName,
      [targetId]: data.placeId
    };
    this.search.form.patchValue(newData);
    this.search.setLocations([]);
  }

  getTargetInputId(type): string {
    return type === 'originCityName' ? 'originPlaceId' : 'destinationPlaceId';
  }

  handleInputEvent(event, inputType, targetInputId): void {
    if (!this.isInteractive) {
      return;
    }

    this.locationPicked = false;

    clearTimeout(this.delayTimer);
    this.delayTimer = setTimeout(_ => {
      this.search.form.controls[targetInputId].setValue('');
      const target: any = event.target;
      this.handleLocationSearch(target.value, inputType);
      this.search.validateForm();
    }, 500);
  }

  // @ts-ignore
  @HostListener('keyup', ['$event'])
  handleKeyboardEvent(event): void {
    if (!this.isInteractive) {
      return;
    }

    this.locationPicked = false;

    if (event.key === 'Enter') {
      this.locationPicked = true;
      this.onSubmitForm();
    }
  }

  onSubmitForm() {
    this.search.validateForm();
    const { value } = this.search.form;
    if (!this.isInteractive) {
      return;
    }
    this.submitButtonClicked = true;

    if (this.search.form.valid) {
      const queryParams = {
        originPlaceId: value.originPlaceId,
        departure: value.departure,
        arrival: value.arrival
      };
      Object.entries(this.chipsService.chips).forEach(el => {
        queryParams[el[0]] = el[1]['selected']
      });
      if (value.destinationPlaceId) {
        queryParams['destinationPlaceId'] = value.destinationPlaceId;
        queryParams['recommended'] = 0;
        this.api.userClickedData = { recommendationBlock: 'ROADTRIP' };
        this.router.navigate([ '/city' ], { queryParams: queryParams});
      } else {
        this.router.navigate([ '/recommendation' ], { queryParams: queryParams});
      }

      this.clearCachedData(value);
      this.search.flightsData = value;
    }
  }

  onDateChange(e) {
    const from = this.parseDate(e.value.begin);
    const to = this.parseDate(e.value.end);
    this.search.setDate(from, to);
    this.search.validateForm();
  }

  parseDate(date): string {
    return moment(date).format('MM/DD/YYYY');
  }

  onClickDelete(e) {
    this.search.clearDestinationData();
    this.search.validateForm();
  }

  clearCachedData(value) {
    if (
      this.search.flightsData.destinationPlaceId !== value.destinationPlaceId ||
      this.search.flightsData.originPlaceId !== value.originPlaceId ||
      this.search.flightsData.departure !== value.departure ||
      this.search.flightsData.arrival !== value.arrival
    ) {
      this.api.clearReservationData();
      this.api.clearSchedulePlansData();
    }
  }

  onClickReverse(e) {
    const { originPlaceId, destinationPlaceId, originCityName, destinationCityName } = this.search.form.value;
    const reversedData = {
      originPlaceId: destinationPlaceId,
      destinationPlaceId: originPlaceId,
      originCityName: destinationCityName,
      destinationCityName: originCityName
    };
    this.search.form.patchValue(reversedData);
  }

  onChipClick({ name, selected }) {
    this.chipsService.chips[name].selected = !this.chipsService.chips[name].selected;
    if (this.isNeedApplyChip(name) && this.router.url.includes(`&${name}=`)) {
      const queryParams = { [name]: !selected};
      this.router.navigate(
        [],
        {
          relativeTo: this.route,
          queryParams,
          queryParamsHandling: 'merge',
        });
    }
  }

  objectValues(obj) {
    return Object.values(obj);
  }

  private isNeedApplyChip(name: string): boolean {
    let isCityChip = this.citiesChips.some(chips => chips === name);
    let isApply = (this.router.url.includes('/recommendation') && !isCityChip) || (this.router.url.includes('/city') && isCityChip);
    return isApply;
  }

  private initCitiesChips(): void {
    SECTIONS.forEach(section => {
      const name = section.title.toLowerCase();
      this.citiesChips.push(name);
    });
  }
}



