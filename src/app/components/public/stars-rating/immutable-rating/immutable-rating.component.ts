import {Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-immutable-rating',
  templateUrl: './immutable-rating.component.html',
  styleUrls: ['./immutable-rating.component.css']
})
export class ImmutableRatingComponent implements OnInit {
  @Input() rating: number;
  @Input() isAddTextShadow: boolean;

  constructor() { }

  ngOnInit() {
    this.rating = Math.round(this.rating);
  }

}
