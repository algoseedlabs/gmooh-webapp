import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EventComponent} from '../event/event.component';

@Component({
  selector: 'app-stars-raiting',
  templateUrl: './stars-raiting.component.html',
  styleUrls: ['./stars-raiting.component.css']
})
export class StarsRaitingComponent implements OnInit {

  @Input() rating: number;
  @Output() ratingClick: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
    this.rating = Math.round(this.rating);
  }

  onClick(rating: number): void {
    this.rating = rating;
    this.ratingClick.emit({
      rating: rating
    });
  }

}
