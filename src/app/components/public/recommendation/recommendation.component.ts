import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RouteService } from '../../../services/route.service';
import { WindowService } from '../../../services/window.service';
import { SearchService } from '../../../services/search.service';
import { SearchChipsService } from '../../../services/search-chips.service';



@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.css']
})
export class RecommendationComponent implements OnInit, OnDestroy {

  windowSub;
  isMobile: boolean;
  public isSearching = true;
  public recommendedPlaces = [
    {
      title: 'ROADTRIP',
      data: null,
    },
    {
      title: 'GETAWAY DEALS',
      data: null,
    }, {
      title: 'TRENDING',
      data: null,
    }];
  public successfulRequest = false;

  @ViewChild('recommendation') recommendation;

  constructor(
    private apiService: ApiService,
    private search: SearchService,
    private chipsService: SearchChipsService,
    private routeService: RouteService,
    private router: Router,
    private route: ActivatedRoute,
    private windowService: WindowService,
  ) { }

  ngOnInit() {
    this.windowSub = this.windowService.windowSize.subscribe(res => {
      this.isMobile = res.width < this.windowService.DESKTOP_WIDTH;
    });

    this.getListOfRecommendation();

  }

  ngOnDestroy() {
    this.windowSub.unsubscribe();
  }

  getListOfRecommendation() {
    this.route.queryParams.subscribe(params => {

      this.chipsService.getChipsStateFromUrl(params);

      if (params.originPlaceId) {
        this.isSearching = true;
        this.resetRecommendationData();
        const searchData = this.apiService.getConvertedSearchRequestData(params);
        this.apiService.getRecommendation(searchData).subscribe(
          res => {
            if (res.status === 'SUCCESS') {
              this.successfulRequest = true;
              const flightPrice = {
                flightPrice: 100
              };
              this.search.flightsData = flightPrice;
              this.recommendedPlaces[0].data = params.roadtrip === 'true' ?  res.response['roadtripList'] : [];
              this.recommendedPlaces[1].data = res.response['getawayDealList'];
              this.recommendedPlaces[2].data = res.response['trendingList'];
              this.isSearching = false;
            } else {
              this.handleError();
            }
            this.scrollToFirstElement();
          },
          err => {
            this.handleError();
          }
        );
      } else {
        this.handleError();
      }
    });

  }

  handleError() {
    this.successfulRequest = false;
    this.isSearching = false;
  }

  resetRecommendationData() {
    this.recommendedPlaces.forEach(item => item.data = null);
  }

  private scrollToFirstElement() {
    if (!this.windowService.isDesktop) {
      const scrollTo = this.recommendation.nativeElement.offsetTop + window.innerHeight;
      window.scrollTo(0, scrollTo);
    }
  }
}
