import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../../services/api.service';
import { SearchService } from '../../../../services/search.service';
import { SwiperDirective } from 'ngx-swiper-wrapper';
import { SwiperService } from '../../../../services/swiper.service';


@Component({
  selector: 'app-recommendation-row',
  templateUrl: './recommendation-row.component.html',
  styleUrls: ['./recommendation-row.component.css']
})
export class RecommendationRowComponent implements OnInit {

  @Input() recommendation;
  @Input() index: number;
  @Input() isMobile: boolean;

  activeIndex = undefined;
  visibleIndexes: Array<number>;

  @ViewChild(SwiperDirective) directiveRef ?: SwiperDirective;


  constructor(
    private router: Router,
    private apiService: ApiService,
    private search: SearchService,
    private swiperService: SwiperService
    ) {
      swiperService.setConfig({ spaceBetween: 0 });
    }

  ngOnInit() {
    this.detectVisibleIndexes(0);
  }

  onIndexChange(index) {
    this.detectVisibleIndexes(index);
  }

  detectVisibleIndexes(index) {
    const visibleIndexes = [];
    for (let i = index; i <= index + this.swiperService.slidesPerView - 1; i++) {
      visibleIndexes.push(i);
    }
    this.visibleIndexes = visibleIndexes;
  }

  onMakeCardActive({ index, isOver }) {
    this.activeIndex = isOver ? index : undefined;
  }

  onCityClick(destinationPlaceId, destinationCityName, title) {
    const queryParams =  { destinationPlaceId, recommended: 1 };

    this.apiService.clearReservationData();
    this.apiService.userClickedData = { recommendationBlock: title };
    this.search.flightsData = { destinationPlaceId };
    this.search.flightsData = { destinationCityName };

    this.router.navigate(['city'], { queryParams, queryParamsHandling: 'merge' }, );
  }

}
