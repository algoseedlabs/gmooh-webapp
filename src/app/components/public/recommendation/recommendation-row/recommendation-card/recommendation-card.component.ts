import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {environment} from '../../../../../../environments/environment';

@Component({
  selector: 'app-recommendation-card',
  templateUrl: './recommendation-card.component.html',
  styleUrls: ['./recommendation-card.component.css']
})
export class RecommendationCardComponent implements OnInit {

  active = false;
  baseUrl = environment.BASE_URL;


  @Input() city;
  @Input() index;
  @Output() onCardActive = new EventEmitter<any>();


  constructor() { }

  ngOnInit() {
  }

  onCardHover(isOver) {
    this.active = !this.active;
    this.onCardActive.emit({ index: this.index, isOver });
  }

  bgImage(i) {
    if (this.city.images) {
      const image = this.city.images[i] || this.city.images[0];
      return this.baseUrl + image;
    } else {
      return this.baseUrl + this.city.image;
    }
  }
}
