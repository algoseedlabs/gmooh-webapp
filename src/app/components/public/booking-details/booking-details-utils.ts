

  export function bound(target: Object, propKey: string | symbol) {
    const originalMethod = (target as any)[propKey] as Function;
    if (typeof originalMethod !== 'function') {
      throw new TypeError('@bound can only be used on methods.');
    }

    if (typeof target === 'function') {
      return {
        value: function () {
          return originalMethod.apply(target, arguments);
        }
      };
    } else if (typeof target === 'object') {
      return {
        get: function () {
          const instance = this;
          Object.defineProperty(instance, propKey.toString(), {
            value: function () {
              return originalMethod.apply(instance, arguments);
            }
          });
          return instance[propKey];
        }
      } as PropertyDescriptor;
    }
  }

