import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../services/api.service';
import {SearchService} from '../../../../services/search.service';

@Component({
  selector: 'app-flight-data',
  templateUrl: './flight-data.component.html',
  styleUrls: ['./flight-data.component.css']
})
export class FlightDataComponent implements OnInit {

  constructor(
    public search: SearchService,
  ) { }

  ngOnInit() {
  }

  get origin() {
    return this.search.flightsData.originCityName;
  }

  get destination() {
    return this.search.flightsData.destinationCityName;
  }

}
