import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material';
import {environment} from '../../../../environments/environment';
import {ApiService} from '../../../services/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GoogleAPIService} from '../../../services/googleAPI.service';
import {IDataGoogle, IGoogleEvent} from '../../../interfaces/data-google';
import {BookingDetailsService} from '../../../services/booking/booking-details.service';
import {UserService} from '../../../services/user/user.service';
import {IPlaceData} from '../../../interfaces/places';
import {PaymentModalComponent} from '../../user/payment-modal/payment-modal.component';
import {WindowService} from '../../../services/window.service';
import {BookingTableService} from '../../../services/booking/booking-table.service';
import {SearchService} from '../../../services/search.service';

declare const require: any;
const Moment = require('moment');
const MomentRange = require('moment-range');


const moment = MomentRange.extendMoment(Moment);

@Component({
    selector: 'app-booking-details',
    templateUrl: './booking-details.component.html',
    styleUrls: ['./booking-details.component.css']
})

export class BookingDetailsComponent implements OnInit, OnDestroy {

    baseUrl = environment.BASE_URL;
    user: any;
    windowSub;

    @ViewChild('bookingDetailsPage') bookingDetailsPage: ElementRef<HTMLElement>;

    constructor(
        public dialog: MatDialog,
        private api: ApiService,
        public googleApi: GoogleAPIService,
        private booking: BookingDetailsService,
        private search: SearchService,
        private table: BookingTableService,
        private activatedRoute: ActivatedRoute,
        private userService: UserService,
        private windowService: WindowService,
        private router: Router
    ) {
    }


    ngOnInit() {

        this.windowSub = this.windowService.windowSize.subscribe(res => {
            this.booking.isMobile = res.width <= this.windowService.BOOKING_MOBILE_VIEW;
        });

        this.booking.pendingTripId = Number(this.activatedRoute.snapshot.paramMap.get('id'));

        const copyTripId = Number(this.activatedRoute.snapshot.paramMap.get('copyTripId'));

        if (this.booking.pendingTripId) {
            this.user = this.userService.userData;
            if (this.user.token) {
                this.api.getTripById(this.booking.pendingTripId, this.user.token).subscribe(data => {
                    if (data.status === 'SUCCESS') {
                        this.handleSavedDataById(data);
                    }
                });
            } else {
                this.booking.clearCacheAndRedirect();
            }
        } else if (copyTripId) {
            this.api.getTripById(copyTripId, '').subscribe(res => {
                this.handleSavedDataById(res, false);
            });
        } else {
            this.table.setInitialData();
            setTimeout(() => {
                this.scrollToFirstElement();
            }, 0);
        }

        this.addSchedulePlansFromLocalStorage();
    }

    ngOnDestroy() {
        this.windowSub.unsubscribe();
        this.booking.resetTripTableData();
    }

    handleSavedDataById(response, isPending = true) {
        if (response.status === 'SUCCESS') {
            const data = response.response;
            console.log('!!!!', data);
            if (!isPending) {
                const today = moment(moment(new Date()).format('MM/DD/YYYY'));
                const departure = moment(data.dateFrom);
                const arrival = moment(data.dateTo);
                const diff = today.diff(departure, 'days');

                if (diff > 0) {
                    const newDateFrom = today;
                    const newDateTo = arrival.add(diff, 'days').format('MM/DD/YYYY');
                    const newDetailsList = data.detailsList.map(item => {
                        const itemDateFrom = item.dateFrom.split(' ');
                        const itemDateTo = item.dateTo.split(' ');
                        const newItemDateFrom = moment(itemDateFrom[0]).add(diff, 'days').format('MM/DD/YYYY') + ' ' + itemDateFrom[1];
                        const newItemDateTo = moment(itemDateTo[0]).add(diff, 'days').format('MM/DD/YYYY') + ' ' + itemDateTo[1];
                        item.dateFrom = newItemDateFrom;
                        item.dateTo = newItemDateTo;
                        return item;
                    });
                    data.dateFrom = newDateFrom;
                    data.dateTo = newDateTo;
                    data.detailsList = newDetailsList;
                }
            }

            this.search.flightsData = {};
            this.booking.generateFlightsData(data);
            this.api.clearReservationData();
            const targetArray = data.unverifiedList || data.detailsList;
            targetArray.forEach(item => {
                this.api.reservationData = {[item.placeId]: item};
            });
            this.table.setInitialData();
            this.table.fillTableWithSavedData(data.detailsList);
            this.scrollToFirstElement();
        } else {
            this.booking.clearCacheAndRedirect();
        }
    }

    get citiesData(): boolean {
        if (this.search.flightsData) {
            return Object.keys(this.search.flightsData).length !== 0;
        } else {
            return false;
        }
    }

    openPaymentDialog(): void {
        this.saveSchedulePlansData();
        this.api.tripEvents = this.booking.createDetailsList();
        this.booking.openPaymentModal(PaymentModalComponent);
    }

    synchronizeWithGoogleCalendar(): void {
        const dataEvents = this.booking.createDetailsList();
        if (dataEvents) {
            const googleEvents: IGoogleEvent[] = this.createGoogleEvents(dataEvents);
            const dataGoogle: IDataGoogle = this.api.getDataGoogle();
            if (dataGoogle && Object.keys(dataGoogle).length !== 0) {
                this.googleApi.sendToCalendar(dataGoogle, googleEvents);
            } else {
                this.googleApi.getDataGoogleAndSendToCalendar(googleEvents);
            }
        }
    }

    createGoogleEvents(dataEvents): IGoogleEvent[] {
        const googleEvents: IGoogleEvent[] = [];
        dataEvents.forEach((event) => {
            const newEvent: IGoogleEvent = {
                summary: event.name,
                start: {
                    dateTime: new Date(event.dateFrom)
                },
                end: {
                    dateTime: new Date(event.dateTo)
                },
                location: event.address,
                description: event.description,
                colorId: this.booking.getGoogleColorId(event.type)
            };
            googleEvents.push(newEvent);
        });
        return googleEvents;
    }

    saveTrip(): void {
        const user = JSON.parse(localStorage.getItem('@gmooh:currentUser'));
        this.saveSchedulePlansData();
        if (user && user.token) {
            const tripData = this.prepareTripForSave();
            this.createTrip(tripData, user.token, true);
        } else {
            this.booking.openLoginModal();
        }
    }

    createTrip(data, token, pending) {
        this.api.createTrip(data, token, pending).subscribe(res => {
            if (res.status === 'SUCCESS') {
                setTimeout(() => {
                    this.booking.clearCacheAndRedirect();
                }, 1000);
                this.booking.openPaymentResponseModal(false, 'Trip successfully added');
            } else {
                this.booking.openPaymentResponseModal(true, `Trip wasn't added. Please try later.`);
            }
        });
    }

    prepareTripForSave() {
        this.api.tripEvents = this.booking.createDetailsList();
        const tripData = this.booking.getTripData();
        tripData['unverifiedList'] = this.createPlans();
        return tripData;
    }

    createPlans() {
        const unverifiedList = [];
        this.table.plans.forEach((plan: IPlaceData) => {
            const image = plan.image ? plan.image : plan.images && plan.images.length ? plan.images[0] : undefined;
            const item = {
                address: plan.address,
                type: plan.type || plan.placeType,
                image,
                name: plan.name || plan.placeName,
                placeId: plan.placeId
            };
            if (item.type === 5) {
                item['dateFrom'] = moment(plan.startDate).format('MM/DD/YYYY HH:MM:SS');
            }
            unverifiedList.push(item);
        });

        return unverifiedList;
    }

    private scrollToFirstElement() {
        if (!this.windowService.isDesktop) {
            const scrollTo = this.bookingDetailsPage.nativeElement.offsetTop;
            window.scrollTo(0, scrollTo);
        }
    }

    private saveSchedulePlansData(): void {
        if (this.isBookingDetailsIsNotSaved()) {
            this.booking.saveSchedulePlansData();
        }
    }

    private addSchedulePlansFromLocalStorage(): void {
        if (this.isBookingDetailsIsNotSaved()) {
            this.table.addSchedulePlansFromLocalStorage();
        }
    }

    private isBookingDetailsIsNotSaved(): boolean {
        return this.router.url === '/booking-details';
    }
}



