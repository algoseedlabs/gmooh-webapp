import {Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BookingDetailsComponent } from '../booking-details.component';
import { BookingDetailsService } from '../../../../services/booking/booking-details.service';
import { BookingTableService } from '../../../../services/booking/booking-table.service';



@Component({
  styleUrls: ['../plan-table/plan-table.component.css', './events-modal.component.css'],
  templateUrl: './events-modal.component.html',
  selector: 'app-events-modal'
})

export class EventsModalComponent {

  constructor(
    public table: BookingTableService,
    public booking: BookingDetailsService,
    public dialogRef: MatDialogRef<BookingDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {}

  chooseEvent(item) {
    this.dialogRef.close({
      item: item
    });
  }
}
