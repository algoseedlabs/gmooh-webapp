import { Component, OnInit, QueryList, AfterViewInit, OnDestroy, ViewChild, ElementRef, ViewChildren, HostListener } from '@angular/core';
import {CdkDrag, CdkDragDrop, CdkDragMove, copyArrayItem, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ResizeEvent } from 'angular-resizable-element';
import { bound } from '../booking-details-utils';
import { map, startWith, switchMap, tap } from 'rxjs/operators';
import { merge, Subscription } from 'rxjs';
import { GoogleAPIService } from '../../../../services/googleAPI.service';
import { BookingDetailsService } from '../../../../services/booking/booking-details.service';
import { BookingTableService } from '../../../../services/booking/booking-table.service';
import { EventsModalComponent } from '../events-modal/events-modal.component';
import { MatDialog } from '@angular/material';

const speed = 10;
const EVENT_MIN_HEIGHT_PX = 50;

@Component({
  selector: 'app-plan-table',
  templateUrl: './plan-table.component.html',
  styleUrls: ['./plan-table.component.css']
})
export class PlanTableComponent implements OnInit, AfterViewInit, OnDestroy {

  private animationFrame: number | undefined;
  private trash = [];
  private resizeStarted = false;
  subs = new Subscription();

  @ViewChild('scrollEl')
  scrollEl: ElementRef<HTMLElement>;

  @ViewChildren(CdkDrag)
  dragEls: QueryList<CdkDrag>;

  @HostListener('window:mousewheel', [ '$event' ]) onMousewheel(event) {
    if (this.resizeStarted) {
      event.preventDefault();
    }
  }

  constructor(
    public googleApi: GoogleAPIService,
    public booking: BookingDetailsService,
    public table: BookingTableService,
    public dialog: MatDialog,
  ) { }

  @bound
  public triggerScroll($event: CdkDragMove) {
    if (this.animationFrame) {
      cancelAnimationFrame(this.animationFrame);
      this.animationFrame = undefined;
    }
    this.animationFrame = requestAnimationFrame(() => this.scroll($event));
  }

  @bound
  private cancelScroll() {
    if (this.animationFrame) {
      cancelAnimationFrame(this.animationFrame);
      this.animationFrame = undefined;
    }
  }

  ngOnInit() {}

  ngAfterViewInit() {
    const onMove$ = this.dragEls.changes.pipe(
      startWith(this.dragEls)
      , map((d: QueryList<CdkDrag>) => d.toArray())
      , map(dragels => dragels.map(drag => drag.moved))
      , switchMap(obs => merge(...obs))
      , tap(this.triggerScroll)
    );

    this.subs.add(onMove$.subscribe());

    const onDown$ = this.dragEls.changes.pipe(
      startWith(this.dragEls)
      , map((d: QueryList<CdkDrag>) => d.toArray())
      , map(dragels => dragels.map(drag => drag.ended))
      , switchMap(obs => merge(...obs))
      , tap(this.cancelScroll)
    );

    this.subs.add(onDown$.subscribe());
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
    this.table.resetInitialData();
  }

  private scroll($event: CdkDragMove) {
    const { y } = $event.pointerPosition;
    const baseEl = this.scrollEl.nativeElement;
    const box = baseEl.getBoundingClientRect();
    const scrollTop = baseEl.scrollTop;
    const top = box.top + -y;
    if (top > 0 && scrollTop !== 0) {
      baseEl.scrollTop = scrollTop - speed * Math.exp(top / EVENT_MIN_HEIGHT_PX);
      this.animationFrame = requestAnimationFrame(() => this.scroll($event));
      return;
    }

    const bottom = y - box.bottom;
    if (bottom > 0 && scrollTop < box.bottom) {
      baseEl.scrollTop = scrollTop + speed * Math.exp(bottom / EVENT_MIN_HEIGHT_PX);
      this.animationFrame = requestAnimationFrame(() => this.scroll($event));
    }
  }

  drop(event: CdkDragDrop<string[]>, index?) {
    const targetKey = event.container.id.split('row-')[1];
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      this.googleApi.setCalendarNotSynchronization(true);

      if (event.previousContainer.id.match('row-')) {
        // dragging from scheduled to plans
        if (!event.container.id.match('row-')) {
          transferArrayItem(
            event.previousContainer.data,
            this.trash,
            event.previousIndex,
            event.currentIndex);
        } else {
          // dragging between scheduled
          transferArrayItem(
            event.previousContainer.data,
            event.container.data,
            event.previousIndex,
            event.currentIndex);
          this.replaceScheduledItems(targetKey);
          this.setItemEndTime(targetKey, index)
        }
      } else {
        // dragging from plans to scheduled
        copyArrayItem(
          JSON.parse(JSON.stringify(event.previousContainer.data)),
          event.container.data,
          event.previousIndex,
          event.currentIndex);
        this.replaceScheduledItems(targetKey);
        this.setItemEndTime(targetKey, index);
        this.googleApi.numberOfScheduledEvents ++;
      }

    }
  }

  replaceScheduledItems(key) {
    if (this.table.scheduledPlans[this.date][key].length > 1) {
      this.table.scheduledPlans[this.date][key].splice(1);
    }
  }

  start(event, item) {
    event.source.element.nativeElement.style.height = EVENT_MIN_HEIGHT_PX + 'px';
  }

  validateCardResize(event: ResizeEvent): boolean {
    return !(event.rectangle.height < EVENT_MIN_HEIGHT_PX);
  }

  onResize(event, index): void {
    !this.resizeStarted && (this.resizeStarted = true);
    const rowsNumber = Math.round(event.rectangle.height / EVENT_MIN_HEIGHT_PX);
    // remove merged items from scheduled
    if (rowsNumber - 1 !== 0) {
      const targetTime = this.table.hours[index + rowsNumber - 1];
      if (this.table.scheduledPlans[this.date][targetTime].length > 0) {
        this.table.scheduledPlans[this.date][targetTime] = [];
      }
    }
  }

  onResizeEnd(event: ResizeEvent, item, i): void {
    this.resizeStarted = false;
    const roundedHeight = Math.round(event.rectangle.height / EVENT_MIN_HEIGHT_PX) * EVENT_MIN_HEIGHT_PX;
    this.table.scheduledPlans[this.date][item][0]['height'] = roundedHeight;
    this.setItemEndTime(item, i);
  }

  onChangeDate(item) {
    this.table.activeDate = this.table.convertDate(item.value);
  }

  setItemEndTime(item, index) {
    const height = this.table.scheduledPlans[this.date][item][0].height || this.table.rowHeight;
    this.table.scheduledPlans[this.date][item][0]['endTime'] = this.getEndTimeByCardHeight(height, index);
  }

  getEndTimeByCardHeight(height, index) {
    const duration = (height / this.table.rowHeight);
    if (this.table.hours[index + duration] === undefined) {
      const diff = duration - (this.table.hours.length - index);
      return this.table.hours[diff];
    }
    return this.table.hours[index + duration];
  }

  get date() {
    return this.table.activeDate;
  }

  showModalWithEvents(hour): void {
    if (this.booking.isMobile) {
      const eventModal = this.dialog.open(EventsModalComponent, {
        width: '95vw',
      });

      eventModal.afterClosed().subscribe(
        data => {
          if (data) {
            this.googleApi.setCalendarNotSynchronization(true);
            this.googleApi.numberOfScheduledEvents ++;
            this.table.scheduledPlans[this.date][hour][0] = JSON.parse(JSON.stringify(data.item));
          }
        }
      );
    }
  }

}
