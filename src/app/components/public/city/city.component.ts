import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Location } from '@angular/common';
import { environment } from '../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MapService } from '../../../services/map.service';
import { MODAL_MOBILE_WIDTH, MODAL_WIDTH } from '../header/header.component';
import { ErrorComponent } from '../@dialogs/error/error.component';
import { IItemOfListOfNamesToSort } from '../../../interfaces/shared-components';
import { ISection } from '../../../interfaces/cities';
import { LIST_OF_NAMES_TO_SORT, SECTIONS } from '../../../app-constants';
import { SearchService } from '../../../services/search.service';
import { WindowService } from '../../../services/window.service';
import { CitiesService } from 'src/app/services/cities.service';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SearchChipsService } from '../../../services/search-chips.service';

const cuttedTextHeight = 50;

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: [ './city.component.css' ],
  animations : [
    trigger('panelState', [
      state('closed', style({ height: cuttedTextHeight + 'px', overflow: 'hidden' })),
      state('open', style({ height: '*' })),
      transition('closed <=> open', animate('300ms ease-in-out')),
    ]),
  ],
})
export class CityComponent implements OnInit {

  listOfNamesToSort: IItemOfListOfNamesToSort[] = LIST_OF_NAMES_TO_SORT;

  isSearching = false;

  sections: ISection[] = SECTIONS;
  cityPlaces: ISection[] = [];

  baseUrl = environment.BASE_URL;
  cityData = {};
  recommendationBlock = '';
  togglerText = 'Show more >';
  descriptionIsFull = false;
  descriptionIsOpen = true;
  queryParams;

  @ViewChild('description') description: ElementRef;
  @ViewChild('city') city: ElementRef;

  constructor(
    private apiService: ApiService,
    private search: SearchService,
    private chipsService: SearchChipsService,
    private location: Location,
    private route: ActivatedRoute,
    private mapService: MapService,
    public dialog: MatDialog,
    public window: WindowService,
    public cities: CitiesService
  ) {
  }

  ngOnInit() {

    this.handleSearch();

    this.sections.forEach((section: ISection) => {
      this.cityPlaces.push(Object.assign({}, section));
    });

  }


  handleSearch() {
    this.route.queryParams.subscribe(params => {
      this.isSearching = true;
      this.queryParams = params;
      this.chipsService.getChipsStateFromUrl(params);
      const searchData = this.apiService.getConvertedSearchRequestData(params);
      const recommended = +params.recommended;
      if (recommended) {
        this.handleGetCities(searchData);
      } else {
        this.apiService.getRecommendation(searchData).subscribe(res => {
          if (res.status === 'SUCCESS' && res.response && res.response.getawayDealList) {
            const flightPrice = {
              flightPrice: 100
            };
            this.search.flightsData = flightPrice;
            this.handleGetCities(searchData);
          } else {
            this.handleError();
          }
          this.scrollToFirstElement();
        });
      }
    });

  }

  handleGetCities(city) {
    this.resetCardsData();

    this.recommendationBlock = this.apiService.userClickedData.recommendationBlock;
    this.apiService.getCities(city).subscribe(
      res => {
        if (res.status === 'SUCCESS') {
          this.isSearching = false;
          const data = res.response;
          this.cityData = data;
          this.fillCardsData(data);
          this.mapService.setCurrentCoords('location', data.cityInfo.latitude, data.cityInfo.longitude);
          this.search.flightsData = {
            country: data.cityInfo.address.split(',')[1],
            description: data.cityInfo.description,
            images: data.cityInfo.images,
            tripId: data.cityInfo.placeId,
            cityId: data.cityInfo.cityInfoId,
            cityName: data.cityInfo.name,
          };
          this.apiService.userClickedData = {
            cityId: data.cityInfo.cityInfoId,
            cityName: data.cityInfo.name
          };

          this.checkDescriptionHeight();
        } else {
          this.handleError();
        }
        this.scrollToFirstElement();
      },
      err => {
        this.handleError();
      }
    );
  }

  handleError() {
    const dialog = this.dialog.open(ErrorComponent, {
      width: !this.window.isDesktop ? MODAL_MOBILE_WIDTH : MODAL_WIDTH,
      data: {
        text: 'Data not found'
      }
    });
    dialog.afterClosed().subscribe(res => {
      this.location.back();
    });
  }

  fillCardsData(data) {
    const targetParam = (index) => this.sections[index].title.toLowerCase();
    this.cityPlaces[0].data = this.queryParams[targetParam(0)] === 'true' ? data.accommodationList : [];
    this.cityPlaces[1].data = this.queryParams[targetParam(1)] === 'true' ? data.restaurantList : [];
    this.cityPlaces[2].data = this.queryParams[targetParam(2)] === 'true' ? data.shopList : [];
    this.cityPlaces[3].data = this.queryParams[targetParam(3)] === 'true' ? data.landmarkList : [];
    this.cityPlaces[4].data = this.queryParams[targetParam(4)] === 'true' ? data.eventList : [];
  }

  checkDescriptionHeight() {
    setTimeout(_ => {
      const fullHeight = this.description.nativeElement.clientHeight;
      this.descriptionIsFull = fullHeight <= cuttedTextHeight + 10;
      if (!this.descriptionIsFull) {
        this.descriptionIsOpen = false;
      }
    }, 0);
  }

  resetCardsData() {
    this.cityPlaces.forEach(item => {
      item.data = null;
    });
  }

  toggleText() {
    this.togglerText = this.togglerText === 'Show more >' ? '< Hide' : 'Show more >';
    this.descriptionIsOpen = !this.descriptionIsOpen;
  }

  private scrollToFirstElement() {
    if (!this.window.isDesktop) {
      const scrollTo = this.city.nativeElement.offsetTop + window.innerHeight;
      window.scrollTo(0, scrollTo);
    }
  }
}
