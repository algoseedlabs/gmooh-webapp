import { Component, Input, OnInit } from '@angular/core';
import { IItemOfListOfNamesToSort } from '../../../../interfaces/shared-components';
import { IPlaceInfo } from '../../../../interfaces/places';

@Component({
    selector: 'app-city-row',
    templateUrl: './city-row.component.html',
    styleUrls: ['./city-row.component.css']
})
export class CityRowComponent implements OnInit {

    @Input() cityPlace;
    @Input() cityId: number;
    @Input() isMobile: boolean;
    @Input() index: number;
    @Input() listOfNamesToSort: IItemOfListOfNamesToSort[];
    @Input() queryParams;

    items;
    maxSearchItems = 5;
    maxSortItems = 5;

    ngOnInit() {
      if (
        this.cityPlace.data &&
        this.cityPlace.data.items &&
        this.cityPlace.data.items.length > 0
      ) {
        this.items = this.cityPlace.data.items;
      }
    }

    setDataItems(data: IPlaceInfo[]): void {
        this.items = data;
    }
}
