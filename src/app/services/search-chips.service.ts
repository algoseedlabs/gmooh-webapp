import { Injectable } from '@angular/core';;
import { SECTIONS } from '../app-constants';

@Injectable({
  providedIn: 'root'
})
export class SearchChipsService {

  chips = {};

  constructor() {
    this.createChipsConfig();
  }

  createChipsConfig() {
    this.chips['roadtrip'] = {
      title: 'Road trip',
        name: 'roadtrip',
        selected: true
    };
    SECTIONS.forEach(el => {
      const name = el.title.toLowerCase();
      this.chips[name] = {
        title: el.title,
        name,
        selected: true
      };
    });
  }

  getChipsStateFromUrl(urlParams) {
    setTimeout(_ => {
      Object.keys(this.chips).forEach(key => {
        this.chips[key].selected = urlParams[key] === 'true';
      });
    }, 0);
  }


}
