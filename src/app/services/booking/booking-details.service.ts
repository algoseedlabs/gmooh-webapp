import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { RECEIPT_LIST, SECTIONS, SUCCESS_MODAL_MOBILE, SUCCESS_MODAL_WIDTH } from '../../app-constants';
import { MatDialog } from '@angular/material';
import { SuccessComponent } from '../../components/public/@dialogs/success/success.component';
import { ErrorComponent } from '../../components/public/@dialogs/error/error.component';
import { MODAL_MOBILE_WIDTH, MODAL_WIDTH } from '../../components/public/header/header.component';
import { LoginComponent } from '../../components/public/auth/login/login.component';
import { ISection } from '../../interfaces/cities';
import { Router } from '@angular/router';
import { ReservationService } from '../reservation.service';
import { BookingTableService } from './booking-table.service';
import {SearchService} from '../search.service';

declare const require: any;
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);

export const FlightsData = {
  originCityName: '',
  destinationCityName: '',
  departure: '',
  arrival: '',
  flightPrice: 0,
  country: '',
  cityName: '',
  description: '',
  images: [],
  id: '',
  placeId: '',
  tripId: '',
  cityId: '',
  originPlaceId: '',
  destinationPlaceId: '',
};

@Injectable({
  providedIn: 'root'
})

export class BookingDetailsService {

  sections: ISection[] = SECTIONS;
  receiptList = RECEIPT_LIST;
  flightPrice = this.search.flightsData.flightPrice || 1;
  totalSum: number = this.flightPrice;
  isMobile;

  pendingTripId: number;

  constructor(
    private api: ApiService,
    private search: SearchService,
    private dialog: MatDialog,
    private router: Router,
    private table: BookingTableService,
    private reservation: ReservationService
  ) {}

  createDetailsList() {
    const detailsList = [];
    Object.entries(this.table.scheduledPlans).forEach(item => {
      Object.entries(item[1]).forEach(el => {
        if (el[1].length > 0) {
          const event = {...el[1][0]};
          event['dateFrom'] = item[0] + ' ' + this.convertTime(el[0]);
          event['dateTo'] = item[0] + ' ' + this.convertTime(el[1][0].endTime);
          detailsList.push(event);
        }
      });
    });
    return detailsList;
  }

  convertTime(time) {
    return moment(time, ['h:mm A']).format('HH:mm:ss');
  }


  getTripData(): any {
    const { country, originCityName, originPlaceId, departure, arrival } = this.search.flightsData;
    const data = {
      city: this.search.flightsData.destinationCityName,
      dateFrom: departure,
      dateTo: arrival,
      destinationPlaceId: this.search.flightsData.tripId,
      detailsList: this.getDetailsList(),
      image: this.search.flightsData.images[0],
      price: this.totalSum,
      transportType: 'air',
      country,
      originCityName,
      originCityPlaceId: originPlaceId,
      flightPrice: this.flightPrice
    };
    if (this.pendingTripId !== undefined) {
      data['id'] = this.pendingTripId;
    }
    return data;
  }

  private getDetailsList() {
    const detailsList = [];
    this.api.tripEvents.forEach(item => {
      detailsList.push(this.getDetail(item));
    });
    return detailsList;
  }

  getDetail(tripEvent): any {
    const image = tripEvent.image ? tripEvent.image : tripEvent.images && tripEvent.images.length ? tripEvent.images[0] : undefined;
    return {
      address: tripEvent.address,
      dateFrom: tripEvent.dateFrom,
      dateTo: tripEvent.dateTo,
      image,
      type: tripEvent.type || tripEvent.placeType,
      placeId: tripEvent.placeId,
      name: tripEvent.name || tripEvent.placeName,
      price: 50,
    };
  }


  resetTripTableData() {
    this.flightPrice = this.search.flightsData.flightPrice ||  1;
    this.receiptList = RECEIPT_LIST;
  }

  calculateEventsQuantity(): void {
    this.api.tripEvents.forEach(item => {
      this.receiptList[item.type]['quantity'] += 1;
    });
  }

  calculateTotalSum() {
    let sum = this.totalSum;
    Object.values(this.receiptList).forEach(item => {
      sum += item.quantity * item.price;
    });
    this.totalSum = +sum.toFixed(2);
  }

  resetEventsQuantityAndTotalSum() {
    Object.values(this.receiptList).forEach(item => {
      item['quantity'] = 0;
      item['price'] = 50;
    });
    this.totalSum = this.flightPrice;
  }

  generateFlightsData(data): void {
    this.search.flightsData = {
      country: data.country,
      images: data.images,
      departure: data.dateFrom,
      arrival: data.dateTo,
      originPlaceId: data.originCityPlaceId,
      tripId: data.destinationPlaceId,
      originCityName: data.originCityName,
      destinationCityName: data.destinationCity,
      flightPrice: data.price
    };
  }

  clearStorage() {
    localStorage.removeItem('@gmooh:reservationData');
    localStorage.removeItem('@gmooh:userClickedData');
    localStorage.removeItem('@gmooh:flightsData');
    localStorage.removeItem('@gmooh:schedulePlansData');
  }

  getGoogleColorId(type): number {
    return this.sections.find(el => el.id === type).googleColorId;
  }

  getPlanColor(plan) {
    const type = plan.type || plan.placeType;
    return this.sections.find(el => el.id === type).color;
  }

  parseDate(date): string {
    return moment(date).format('MM/DD/YYYY');
  }

  openPaymentModal(PaymentModalComponent) {
    this.dialog.open(PaymentModalComponent, {
      width: '70vw',
      data: {
        dateFrom: this.parseDate(this.table.daysRange[0]._d),
        dateTo: this.parseDate(this.table.daysRange[this.table.daysRange.length - 1]._d),
        numberOfDays: this.table.daysRange.length,
        isMobile: this.isMobile,
        id: this.pendingTripId || undefined
      }
    });
  }

  openPaymentResponseModal(isError, text) {
    const targetComponent = !isError ? SuccessComponent : ErrorComponent;
    this.dialog.open(targetComponent, {
      width: this.isMobile ? SUCCESS_MODAL_MOBILE : SUCCESS_MODAL_WIDTH,
      data: {
        isMobile: this.isMobile,
        text: text
      }
    });
  }

  openLoginModal() {
    this.dialog.open(LoginComponent, {
      width: this.isMobile ? MODAL_MOBILE_WIDTH : MODAL_WIDTH,
      data: {
        isMobile: this.isMobile
      }
    });
  }

  clearCacheAndRedirect() {
    this.router.navigate([ '/main' ]);
    this.clearStorage();
    this.reservation.reservationListIsEmpty.next(true);
  }

  navigateToCities() {
    const { originPlaceId, destinationPlaceId, departure, arrival } = this.search.flightsData;
    const queryParams = { originPlaceId, departure, arrival, destinationPlaceId, recommended: 0 };
    this.router.navigate([ 'city' ], { queryParams: queryParams});
  }

  public saveSchedulePlansData(): void {
    const schedulePlansData = {};
    for (const scheduleDate in this.table.scheduledPlans) {
      const scheduleHours = this.getScheduleHoursPlaneOnDay(this.table.scheduledPlans[scheduleDate]);
      if (scheduleHours.length) {
        schedulePlansData[scheduleDate] = this.generateScheduleOnDayByHour(this.table.scheduledPlans[scheduleDate], scheduleHours, scheduleDate);
      }
    }
    this.api.addSchedulePlanData(schedulePlansData);
  }

  private getScheduleHoursPlaneOnDay(scheduleDate): Array<string> {
    const scheduleHours = new Array();
    for (const scheduleHour in scheduleDate) {
      if (scheduleDate[scheduleHour].length) { scheduleHours.push(scheduleHour); }
    }
    return scheduleHours;
  }

  private generateScheduleOnDayByHour(scheduleDate, scheduleHours, date) {
    const result = {};
    scheduleHours.forEach(hour => {
      result[hour] = {
          placeId: scheduleDate[hour][0].placeId,
          hour: hour,
          date: date
        };
    });
    return result;
  }
}
