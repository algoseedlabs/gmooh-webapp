import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { GoogleAPIService } from '../googleAPI.service';
import {BookingDetailsService} from './booking-details.service';
import {SearchService} from '../search.service';


declare const require: any;
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);

@Injectable({
  providedIn: 'root'
})
export class BookingTableService {

  rowHeight = 50;
  activeDate;
  hours = [];
  daysRange = [];
  plans = [];
  scheduledPlans = {};

  constructor(
    private api: ApiService,
    public googleApi: GoogleAPIService,
    private search: SearchService
  ) {}

  setInitialData() {
    this.generatePlansData();
    this.generateDaysRange();
    this.generateHoursRange();
    this.generateScheduledFields();
    this.activeDate = this.convertDate(this.daysRange[0]['_d']);
  }

  resetInitialData() {
    this.plans = [];
    this.daysRange = [];
    this.hours = [];
    this.scheduledPlans = {};
    this.activeDate = undefined;
  }

  generatePlansData() {
    this.plans = [];
    // tslint:disable-next-line:forin
    for (const item in this.api.reservationData) {
      this.plans.push(this.api.reservationData[item]);
    }
  }

  generateDaysRange() {
    const { departure, arrival } = this.search.flightsData;
    const range = moment.range(moment(departure), moment(arrival));
    this.daysRange = Array.from(range.by('day'));
  }

  generateHoursRange() {
    const hoursOfNight = [];
    const hoursOfDay = [];
    for (let i = 1; i <= 12; i++) {
      hoursOfNight.push((i === 1 ? 12 : i - 1 ) + ' AM');
      hoursOfDay.push((i === 1 ? 12 : i - 1 ) + ' PM');
    }
    this.hours = [...hoursOfNight].slice(4).concat(hoursOfDay).concat([...hoursOfNight].slice(0, 4));
  }

  generateScheduledFields() {
    for (let d = 0; d < this.daysRange.length; d ++) {
      const item = {};
      for (let i = 0; i < this.hours.length; i++) {
        item[this.hours[i]] = [];
      }
      const date = this.convertDate(this.daysRange[d]['_d']);
      this.scheduledPlans[date] = item;
    }
  }

  removeItemFromScheduled(key) {
    this.scheduledPlans[this.activeDate][key] = [];
    this.googleApi.numberOfScheduledEvents --;
    this.googleApi.setCalendarNotSynchronization(true);
  }

  deleteItemFromPlans(id) {
    this.plans = this.plans.filter(item => item.placeId !== id);
    this.api.deleteItemFromReservationData(id);
  }

  convertDate(date) {
    return moment(date).format('MM/DD/YYYY');
  }

  convertTime(time) {
    return moment(time, ['HH:mm:ss']).format('h A');
  }

  fillTableWithSavedData(data) {
    console.log(data)
    data.forEach(item => {
      if (item.dateFrom && item.dateTo) {
        const itemCopy = {...item};
        const dateFrom = item.dateFrom.split(' ');
        const dateTo = item.dateTo.split(' ');
        const timeFrom = this.convertTime(dateFrom[1]);
        console.log(timeFrom, this.scheduledPlans, dateFrom[0])
        itemCopy['endTime'] = this.convertTime(dateTo[1]);
        itemCopy['height'] = this.calculateCardHeight(item);
        this.scheduledPlans[dateFrom[0]][timeFrom][0] = itemCopy;
      } else {
        console.log('no dates provided');
      }
    });
  }

  calculateCardHeight(item) {
    const duration = moment.duration(moment(item.dateTo).diff(moment(item.dateFrom)));
    const hoursDiff = duration.asHours();
    let height;
    if (hoursDiff === 0) {
      height = this.rowHeight;
    } else if (hoursDiff > 0) {
      height = hoursDiff * this.rowHeight;
    } else {
      height = (hoursDiff + 12) * this.rowHeight;
    }
    return height;
  }

  addSchedulePlansFromLocalStorage() {
    if (!Object.keys(this.api.schedulePlansData).length) { return; }

    for (let scheduleData in this.api.schedulePlansData) {
      for (let scheduleHour in this.api.schedulePlansData[scheduleData]) {
        if((scheduleData in this.scheduledPlans) && (scheduleHour in this.scheduledPlans[scheduleData])) {
          this.scheduledPlans[scheduleData][scheduleHour] = [this.api.reservationData[this.api.schedulePlansData[scheduleData][scheduleHour].placeId]];
        }
      }
    }
  }
}
