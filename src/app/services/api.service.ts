import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLS } from '../api-urls';
import { Recommendation } from '../interfaces/recommendation';
import { Email } from '../interfaces/email';
import { Trip } from '../interfaces/trip';
import { Registration } from '../interfaces/registration';
import { Login } from '../interfaces/login';
import { Profile } from 'selenium-webdriver/firefox';
import { Review } from '../interfaces/review';
import { Comment } from '../interfaces/comment';
import { IOptionsForSearchPlaces, IOptionsForSortByCityPlaces } from '../interfaces/shared-components';
import { IDataGoogle } from '../interfaces/data-google';
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  private _userClickedData = JSON.parse(localStorage.getItem('@gmooh:userClickedData')) || {};
  private _tripEvents = [];
  private _reservationData = JSON.parse(localStorage.getItem('@gmooh:reservationData')) || {};
  private _schedulePlansData = JSON.parse(localStorage.getItem('@gmooh:schedulePlansData')) || {};
  private dataGoogle: IDataGoogle = JSON.parse(localStorage.getItem('@gmooh:dataGoogle')) || {};

  authHttpOptions = (token) => {

    if (!token) {
      return {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };
    } else {
      return {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
        })
      };
    }
  }


  constructor(private http: HttpClient) {
  }

  getDataGoogle(): IDataGoogle {
    return this.dataGoogle;
  }

  setDataGoogle(data: IDataGoogle): void {
    this.dataGoogle = Object.assign({ ...this.dataGoogle }, data);
    localStorage.setItem('@gmooh:dataGoogle', JSON.stringify(this.dataGoogle));
  }

  clearDataGoogle(): void {
    for (const key of Object.getOwnPropertyNames(this.dataGoogle)) {
      delete this.dataGoogle[key];
    }
    localStorage.removeItem('@gmooh:dataGoogle');
  }

  getConvertedSearchRequestData(data) {
    return {
      dateFrom: data.departure,
      dateTo: data.arrival,
      originPlaceId: data.originPlaceId,
      destinationPlaceId: data.destinationPlaceId,
    };
  }

  set userClickedData(data) {
    this._userClickedData = Object.assign({ ...this._userClickedData }, data);
    localStorage.setItem('@gmooh:userClickedData', JSON.stringify(this._userClickedData));
  }

  get userClickedData() {
    return this._userClickedData;
  }

  get tripEvents() {
    return this._tripEvents;
  }

  set tripEvents(data) {
    this._tripEvents = data;
  }

  get reservationData() {
    return this._reservationData;
  }

  set reservationData(data) {
    this._reservationData = Object.assign({ ...this._reservationData }, data);
    localStorage.setItem('@gmooh:reservationData', JSON.stringify(this._reservationData));
  }

  clearReservationData() {
    this._reservationData = {};
    localStorage.removeItem('@gmooh:reservationData');
  }

  get schedulePlansData() { return this._schedulePlansData; }

  addSchedulePlanData(data) {
    this._schedulePlansData = data;
    localStorage.setItem('@gmooh:schedulePlansData', JSON.stringify(this._schedulePlansData));
  }

  clearSchedulePlansData() {
    this._schedulePlansData = {};
    localStorage.removeItem('@gmooh:schedulePlansData');
  }

  deleteItemFromReservationData(id) {
    const reserved = { ...this._reservationData };
    const updatedReserved = {};
    Object.keys(reserved).forEach(item => {
      if (item !== id) {
        updatedReserved[item] = reserved[item];
      }
    });
    if (Object.keys(updatedReserved).length === 0) {
      this._reservationData = {};
      localStorage.removeItem('@gmooh:reservationData');
    } else {
      localStorage.setItem('@gmooh:reservationData', JSON.stringify(updatedReserved));
      this._reservationData = { ...updatedReserved };
    }

  }

  // api requests

  getUser(id: number, token): Observable<any> {
    return this.http.get<any>(URLS.getProfile + id, this.authHttpOptions(token));
  }

  updateUser(profile: Profile, token): Observable<any> {
    return this.http.put<any>(URLS.editProfile, JSON.stringify(profile), this.authHttpOptions(token));
  }

  sendAvatar(avatar: any): Observable<any> {
    return this.http.post<any>(URLS.avatar, avatar);
  }

  changeAvatar(avatar: any, token): Observable<any> {
    return this.http.put<any>(URLS.changeAvatar, JSON.stringify(avatar), this.authHttpOptions(token));
  }

  login(login: Login): Observable<any> {
    return this.http.post<any>(URLS.login, JSON.stringify(login), this.httpOptions);
  }

  sendEmail(email: Email): Observable<any> {
    return this.http.post<any>(URLS.welcome, JSON.stringify(email), this.httpOptions);
  }

  searchLocation(location: string): Observable<any> {
    return this.http.post<any>(URLS.location + location, this.httpOptions);
  }

  getRecommendation(recommendation): Observable<any> {
    return this.http.post<any>(URLS.recommendation, JSON.stringify(recommendation), this.httpOptions);
  }

  getCities(city): Observable<any> {
    return this.http.post<any>(URLS.citiesId, JSON.stringify(city), this.httpOptions);
  }

  getEvents(url: string, placeId: string): Observable<any> {
    return this.http.post<any>(url + placeId, this.httpOptions);
  }

  getMorePlaces(morePlaces): Observable<any> {
    return this.http.post<any>(URLS.cityMorePlaces, JSON.stringify(morePlaces), this.httpOptions);
  }

  getMoreEvents(moreEvents): Observable<any> {
    return this.http.post<any>(URLS.cityMoreEvents, JSON.stringify(moreEvents), this.httpOptions);
  }

  createTrip(trip, token, pending = false): Observable<any> {
    return this.http.post<any>(URLS.createTrip(pending), JSON.stringify(trip), this.authHttpOptions(token));
  }

  getTripList(id: number, page, size, token): Observable<any> {
    return this.http.get<any>(URLS.tripList(id, page, size), this.authHttpOptions(token));
  }

  getSingleTrip(id: number, token): Observable<any> {
    const url = !token ? URLS.singleTrip : URLS.singleSecureTrip;
    return this.http.get<any>(url + id, this.authHttpOptions(token));
  }

  sendRegData(data: Registration): Observable<any> {
    return this.http.post<any>(URLS.registration, JSON.stringify(data), this.httpOptions);
  }

  sendTripReview(review: Review, token): Observable<any> {
    return this.http.post<any>(URLS.sendReview, JSON.stringify(review), this.authHttpOptions(token));
  }

  getTripReviews(id: number, token): Observable<any> {
    return this.http.get<any>(URLS.reviews + id, this.authHttpOptions(token));
  }

  sendCommentReview(comment: Comment, token): Observable<any> {
    return this.http.post<any>(URLS.sendComment, JSON.stringify(comment), this.authHttpOptions(token));
  }

  forgotPassword(email): Observable<any> {
    return this.http.post<any>(URLS.forgotPassword, JSON.stringify(email), this.httpOptions);
  }

  likeReview(reviewId: number, token): Observable<any> {
    return this.http.post<any>(URLS.likeReview + reviewId, '', this.authHttpOptions(token));
  }

  dislikeReview(reviewId: number, token): Observable<any> {
    return this.http.post<any>(URLS.dislikeReview + reviewId, '', this.authHttpOptions(token));
  }

  likeComment(commentId: number, token): Observable<any> {
    return this.http.post<any>(URLS.likeComment + commentId, '', this.authHttpOptions(token));
  }

  dislikeComment(commentId: number, token): Observable<any> {
    return this.http.post<any>(URLS.dislikeComment + commentId, '', this.authHttpOptions(token));
  }

  likeTrip(tripId: number, token): Observable<any> {
    return this.http.post<any>(URLS.likeTrip + tripId, '', this.authHttpOptions(token));
  }

  loginWithSocial(data): Observable<any> {
    // return this.http.post<any>( URLS.loginWithSocial, JSON.stringify(data), this.authHttpOptions(data.authToken));
    return this.http.post<any>(URLS.loginWithSocial, JSON.stringify(data), this.httpOptions);

  }

  searchCityPlaces(data: IOptionsForSearchPlaces, token?: string): Observable<any> {
    return this.http.post<any>(URLS.searchCityPlaces, JSON.stringify(data), this.authHttpOptions(token));
  }

  sortByCityPlaces(data: IOptionsForSortByCityPlaces, token?: string): Observable<any> {
    return this.http.post<any>(URLS.sortByCityPlaces, JSON.stringify(data), this.authHttpOptions(token));
  }

  getTripById(id: number, token): Observable<any> {
    return this.http.get<any>(URLS.getTripById + id, this.authHttpOptions(token));
  }

  editMetaTags(data): Observable<any> {
    return this.http.post<any>(URLS.editMetaTags, JSON.stringify(data), this.httpOptions);
  }
}
