import { Injectable } from '@angular/core';
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class RouteService {

  private _isBookingDetailsPage = false;
  private _isMainPage = false;

  constructor(
    private router: Router,
  ) { }

  get isRecommendationPage() {
    return this.router.url.includes('/recommendation');
  }

  get isEventPage() {
    return this.router.url.includes('/event');
  }

  get isCityPage() {
    return this.router.url.includes('/city');
  }

  get isBookingDetailsPage() {
    this._isBookingDetailsPage = this.router.url === '/booking-details';
    return this._isBookingDetailsPage;
  }

  get isMainPage() {
    this._isMainPage = this.router.url === '/';
    return this._isMainPage;
  }


}
