import { Injectable, NgZone, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { AuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { SUCCESS_MODAL_MOBILE, SUCCESS_MODAL_WIDTH } from '../app-constants';
import { IDataGoogle, IGoogleEvent } from '../interfaces/data-google';
import { MatDialog } from '@angular/material';
import { SuccessComponent } from '../components/public/@dialogs/success/success.component';
import { DeviceDetectorService } from 'ngx-device-detector';
import { environment } from '../../environments/environment';

declare var gapi: any;

@Injectable({
  providedIn: 'root'
})

export class GoogleAPIService implements OnInit {

  private googleAPIKey: string = environment.googleKey;
  private __calendarNotSynchronization: boolean;
  private isMobile: boolean;
  numberOfScheduledEvents = 0;

  constructor(private deviceDetectorService: DeviceDetectorService,
              private http: HttpClient,
              private apiService: ApiService,
              public dialog: MatDialog,
              private zone: NgZone,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.isMobile = this.deviceDetectorService.isMobile();
  }

  getCalendarNotSynchronization(): boolean {
    return this.__calendarNotSynchronization;
  }

  setCalendarNotSynchronization(calendarNotSynchronization: boolean): void {
    this.__calendarNotSynchronization = calendarNotSynchronization;
  }

  sendToCalendar(dataGoogle: IDataGoogle, googleEvents: IGoogleEvent[]): void {
    if (dataGoogle && googleEvents) {
      gapi.auth.setToken({ access_token: dataGoogle.authToken });
      gapi.client.setApiKey(this.googleAPIKey);
      gapi.client.load('calendar', 'v3', () => {
        const batch = gapi.client.newBatch();
        googleEvents.map((r, j) => {
          batch.add(gapi.client.calendar.events.insert({
            calendarId: `primary`,
            resource: googleEvents[j]
          }));
        });
        batch.then((response) => {
          if (response.status === 200 && response.result) {
            const firstObject = response.result[Object.keys(response.result)[0]];
            if (firstObject.status === 401) {
              this.getDataGoogleAndSendToCalendar(googleEvents);
            } else if (firstObject.status === 200) {
              this.zone.run(() => {
                this.openSuccessModal( 'You trip successfully synchronized with google calendar!');
                this.setCalendarNotSynchronization(false);
              });
            }
          }
        });
      });
    }

  }

  openSuccessModal(message: string): void {
    this.dialog.open(SuccessComponent, {
      width: this.isMobile ?  SUCCESS_MODAL_MOBILE : SUCCESS_MODAL_WIDTH,
      data: {
        isMobile: this.isMobile,
        text: message,
      }
    });
  }

  getDataGoogleAndSendToCalendar(googleEvents: IGoogleEvent[]): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((res: SocialUser) => {
      const dataGoogle: IDataGoogle = {
        authToken: res.authToken,
        email: res.email
      };
      this.apiService.setDataGoogle(dataGoogle);
      this.sendToCalendar(dataGoogle, googleEvents);
    });
  }
}
