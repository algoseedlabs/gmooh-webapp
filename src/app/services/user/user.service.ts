import {Inject, Injectable} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { AvatarService } from './avatar.service';
import { ApiService } from '../api.service';
import { SuccessComponent } from '../../components/public/@dialogs/success/success.component';
import { ErrorComponent } from '../../components/public/@dialogs/error/error.component';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  genders = ['male', 'female']
  private _userData = JSON.parse(localStorage.getItem('@gmooh:currentUser')) || '';
  loggedInState = new BehaviorSubject(!!this._userData);

  constructor(
    private avatar: AvatarService,
    private api: ApiService,
    @Inject(MAT_DIALOG_DATA) public data,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<any>,
  ) {
  }


  get userData() {
    return this._userData;
  }

  set userData(data) {
    this._userData = Object.assign({ ...this._userData }, data);
    localStorage.setItem('@gmooh:currentUser', JSON.stringify(this._userData));
  }

  clearUserData() {
    this._userData = '';
    localStorage.removeItem('@gmooh:currentUser');
  }

  updateUserProfile(data) {
    const { token } = this._userData;
    console.log('update', data);
    if (this.avatar.formattedAvatar === undefined) {
      this.api.updateUser(data, token).subscribe(res => {
          if (res.status === 'SUCCESS') {
            this.handleSuccessfulUpdate(res);
          } else {
            this.handleFailedUpdate(res.status);
          }
        });

      } else {
        forkJoin(
          this.api.changeAvatar({ file: this.avatar.formattedAvatar }, token),
          this.api.updateUser(data, token),
        ).subscribe(([ res1, res2 ]) => {
          console.log(res1, res2);
            if (res1.status === 'SUCCESS' && res2.status === 'SUCCESS') {
              res2.response.avatar = res1.response;
              this.handleSuccessfulUpdate(res2);
            } else {
              this.handleFailedUpdate(res2.status);
            }
          }
        );
    }
  }

  prepareDataForSave(data) {
    const formattedData = {...data};
    const { phoneNumber, gender, country } = formattedData;
    formattedData.gender = gender === 'male' ? 1 : 0;

    if (phoneNumber) {
      formattedData.phoneNumber = Number(formattedData.phoneNumber);
    } else {
      delete formattedData.phoneNumber;
    }

    if (!country) {
      delete formattedData.country;
    }
    return formattedData;

  }

  onChooseAvatar(event, cb) {
    const target: any = event.target;
    if (target.files && target.files[0]) {
      const file = target.files[0];
      const reader = new FileReader();
      reader.onload = _ => cb(reader.result);
      reader.readAsDataURL(file);
    }
  }

  handleSuccessfulUpdate(data) {
    this.userData = data.response;
    this.avatar.formattedAvatar = undefined;
    this.avatar.setAvatarUrl(data.response);
    this.openResponseModal({ data: data.status, message: 'User data save' });
  }

  handleFailedUpdate(error) {
    this.openResponseModal({ data: error,  type: 'error' });
  }

  openResponseModal(config): void {
    const failureText = 'Failure. Try it later.';
    const targetComponent = config.type !== 'error' ? SuccessComponent : ErrorComponent;
    this.dialog.open(targetComponent, {
      width: '530px',
      data: {
        text: config.message || failureText,
        status: config.data.status
      }
    });
  }

}
