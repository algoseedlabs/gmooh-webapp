import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AvatarService {

  avatarSrc = 'assets/images/user-icon.png';
  formattedAvatar;
  baseUrl = environment.BASE_URL;
  placeholders = {
    male: 'assets/images/profile_avatar_male.png',
    female: 'assets/images/profile_avatar_female.png'
  };

  constructor() { }

  setAvatarUrl(currentValue) {
    const { avatar, gender } = currentValue;
    if (avatar) {
      this.avatarSrc = avatar.includes('http') ? avatar : this.baseUrl + avatar;
    } else {
      this.avatarSrc = this.placeholders[gender] || this.placeholders.male;
    }
  }
}
