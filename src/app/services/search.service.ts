import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SECTIONS } from '../app-constants';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  _flightsData = JSON.parse(localStorage.getItem('@gmooh:flightsData')) || {};
  form = new FormGroup({
    originPlaceId: new FormControl('', [ Validators.required ]),
    destinationPlaceId: new FormControl(''),
    originCityName: new FormControl('', [ Validators.required ]),
    destinationCityName: new FormControl(''),
    departure: new FormControl('', [ Validators.required ]),
    arrival: new FormControl('', [ Validators.required ]),
  });
  locations = [];
  originCityIsValid: boolean;
  destinationCityIsValid: boolean;
  dateIsValid: boolean;

  chips = {
    roadtrip: {
      title: 'Road trip',
      name: 'roadtrip',
      selected: false
    }
  };

  constructor() {
    SECTIONS.forEach(el => {
      const name = el.title.toLowerCase();
      this.chips[name] = {
        title: el.title,
        name,
        selected: false
      };
    });
  }

  createChipsConfig() {

  }

  set flightsData(data) {
    this._flightsData = Object.assign({ ...this._flightsData }, data);
    localStorage.setItem('@gmooh:flightsData', JSON.stringify(this._flightsData));
  }

  get flightsData() {
    return this._flightsData;
  }

  setLocations(data) {
    this.locations = data;
  }

  setDate(from, to) {
    this.form.controls.departure.setValue(from);
    this.form.controls.arrival.setValue(to);
  }

  clearDestinationData() {
    this.form.controls.destinationCityName.setValue('');
    this.form.controls.destinationPlaceId.setValue('');
  }

  clearOriginData() {
    this.form.controls.originCityName.setValue('');
    this.form.controls.originPlaceId.setValue('');
  }

  validateForm() {
    this.checkOriginValidation();
    this.checkDestinationValidation();
    this.checkDateValidation();
  }

  private checkDateValidation() {
    const { arrival, departure } = this.form.controls;
    this.dateIsValid = arrival.valid && departure.valid;
  }

  private checkOriginValidation() {
    const { originCityName, originPlaceId } = this.form.controls;
    this.originCityIsValid = originCityName.valid && originPlaceId.valid;
  }

  private checkDestinationValidation() {
    const { destinationCityName, destinationPlaceId } = this.form.controls;
    let validState = true;
    if (destinationCityName.value && destinationCityName.value.length > 0) {
      validState = destinationPlaceId.value.length > 0;
    }
    this.destinationCityIsValid = validState;
  }

  // getCurrentAddress(): void {
  //   if (navigator.geolocation) {
  //     navigator.geolocation.getCurrentPosition(position => {
  //       const lat = position.coords.latitude;
  //       const lng = position.coords.longitude;
  //       const google_map_pos = new google.maps.LatLng(lat, lng);
  //       const google_maps_geocoder = new google.maps.Geocoder();
  //       google_maps_geocoder.geocode({ 'latLng': google_map_pos }, (results, status) => {
  //           if (status === google.maps.GeocoderStatus.OK && results[0]) {
  //             let formatted_address: string;
  //             for (let i = 0; i < results.length - 1; i++) {
  //               for (let j = 0; j < results[i].types.length - 1; j++) {
  //                 if (results[i].types[j] === 'locality') {
  //                   formatted_address = results[i].formatted_address;
  //                   break;
  //                 }
  //               }
  //             }
  //             this.apiService.searchLocation(formatted_address).subscribe(res => {
  //                 if (res && res.response) {
  //                   this.searchForm.controls.location.setValue(res.response[0].placeName);
  //                   this.onSelectOption('location', res.response[0]);
  //                 } else {
  //                   console.log(res.errorMessage);
  //                 }
  //               }
  //             );
  //           }
  //         }
  //       );
  //
  //     });
  //   }
  // }
}
