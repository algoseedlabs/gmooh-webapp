import {Injectable, ViewChild} from '@angular/core';
import {SwiperConfigInterface, SwiperDirective} from 'ngx-swiper-wrapper';
import {WindowService} from './window.service';

@Injectable({
  providedIn: 'root'
})
export class SwiperService {

  windowSub;
  isMobile;
  isTablet;
  isDesktop;

  config: SwiperConfigInterface = {};

  @ViewChild(SwiperDirective) directiveRef ?: SwiperDirective;

  constructor(
    private window: WindowService
  ) {
    this.detectScreenSize();
    this.setWindowListener();
  }


  setConfig({ spaceBetween }) {
    this.config = {
      a11y: true,
      direction: 'horizontal',
      keyboard: true,
      mousewheel: false,
      scrollbar: false,
      navigation: true,
      pagination: false,
      watchOverflow: true,
      preloadImages: true,
      spaceBetween: spaceBetween,
      slidesPerView: this.setNumberOfSlides()
    };
  }

  setWindowListener() {
    if (this.windowSub === undefined) {
      this.windowSub = this.window.windowSize.subscribe(res => {
        const isMobile = res.width <= this.window.SWIPER_MOBILE_VIEW;
        const isTablet = res.width < this.window.SWIPER_TABLET_VIEW && res.width > this.window.SWIPER_MOBILE_VIEW;
        const isDesktop = res.width >= this.window.SWIPER_TABLET_VIEW && res.width < this.window.SWIPER_WIDE_DESKTOP;
        if (this.isMobile !== isMobile || this.isTablet !== isTablet || this.isDesktop !== isDesktop) {
          this.config.slidesPerView = isMobile ? 1 : isTablet ? 2 : isDesktop ? 4 : 5;
          setTimeout(_ => {
            if (this.directiveRef) {
              this.directiveRef.update();
            }
          }, 500);

        }
        this.isMobile = isMobile;
        this.isTablet = isTablet;
        this.isDesktop = isDesktop;
      });
    }

  }

  detectScreenSize() {
    this.isMobile = window.innerWidth <= this.window.SWIPER_MOBILE_VIEW;
    this.isTablet = window.innerWidth < this.window.SWIPER_TABLET_VIEW && window.innerWidth > this.window.SWIPER_MOBILE_VIEW;
    this.isDesktop = window.innerWidth >= this.window.SWIPER_TABLET_VIEW && window.innerWidth < this.window.SWIPER_WIDE_DESKTOP;
  }

  setNumberOfSlides() {
    return this.isMobile ? 1 : this.isTablet ? 2 : this.isDesktop ? 4 : 5;
  }

  get slidesPerView() {
    return this.config.slidesPerView;
  }
}
