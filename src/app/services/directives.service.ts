import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class DirectivesService {

  private _rightTrace = [];
  private _leftTrace = [];

  constructor() { }


  get rightTrace(): any {
    return this._rightTrace;
  }

  set rightTrace(coords) {
    this._rightTrace.push(coords);
  }

  get leftTrace(): any {
    return this._leftTrace;
  }

  set leftTrace(coords) {
    this._leftTrace.push(coords);
  }

  resetTraces() {
    this._rightTrace = [];
    this._leftTrace = [];
  }

}
