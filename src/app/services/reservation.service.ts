import { Injectable } from '@angular/core';
import { SuccessComponent } from '../components/public/@dialogs/success/success.component';
import { ApiService } from './api.service';
import { MatDialog } from '@angular/material';
import { IPlaceData } from '../interfaces/places';
import { SUCCESS_MODAL_MOBILE, SUCCESS_MODAL_WIDTH } from '../app-constants';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ReservationService {

  reservationData = Object.keys(this.apiService.reservationData).length > 0;
  reservationListIsEmpty = new BehaviorSubject(!this.reservationData);

  constructor(
    private apiService: ApiService,
    private dialog: MatDialog
  ) {}

  eventReservation(eventData: IPlaceData, isMobile: boolean) {
    this.reservationListIsEmpty.next(false);
    const newReservationData = {};
    newReservationData[eventData.placeId] = { ...eventData };
    this.apiService.reservationData = { ...newReservationData };
    this.showSuccessModal(isMobile);
  }

  showSuccessModal(isMobile) {
    this.dialog.open(SuccessComponent, {
      width: isMobile ? SUCCESS_MODAL_MOBILE : SUCCESS_MODAL_WIDTH,
      data: {
        isMobile: isMobile,
        text: 'Reservation added'
      }
    });
  }

}
