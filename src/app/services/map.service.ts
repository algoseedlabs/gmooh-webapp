import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MapService {
  public showMarker;
  public onMapGotCoords = new Observable(observer => {
    this.showMarker = observer;
    return this.showMarker.next(false);
  });

  public currentCoords = {
    location: {
      lat: 0,
      lng: 0,
    },
    marker: {
      lat: 0,
      lng: 0,
    }
  };


  constructor() {}

  getCurrentCoords(): Observable<any> {
    return of (this.currentCoords);
  }

  setCurrentCoords(type, lat, lng): void {
    if (type === 'location') {
      this.currentCoords.location.lat = lat;
      this.currentCoords.location.lng = lng;
    } else {
      this.currentCoords.marker.lat = lat;
      this.currentCoords.marker.lng = lng;
      this.showMarker.next(true);
    }
  }

}
