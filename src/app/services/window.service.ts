import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WindowService {

  DESKTOP_WIDTH = 1024;
  SWIPER_MOBILE_VIEW = 375;
  SWIPER_TABLET_VIEW = 768;
  SWIPER_WIDE_DESKTOP = 1500;
  BOOKING_MOBILE_VIEW = 768;
  isDesktop;

  private initialWindowSize = {
    width: window.innerWidth,
    height: window.innerHeight
  };

  private initialPageOffset = {
    offsetY: 0,
  };

  windowSize = new BehaviorSubject(this.initialWindowSize);
  pageOffsetY = new BehaviorSubject(this.initialPageOffset);

  constructor() {
    this.windowSize.subscribe(res => {
      this.isDesktop = res.width >= this.DESKTOP_WIDTH;
    });
  }


}
