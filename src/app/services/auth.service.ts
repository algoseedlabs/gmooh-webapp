import { Injectable } from '@angular/core';
import {ProfileMoreInfoComponent} from '../components/public/@dialogs/profile-more-info/profile-more-info.component';
import {MatDialog, MatDialogRef} from '@angular/material';
import { WindowService } from './window.service';
import {SuccessComponent} from '../components/public/@dialogs/success/success.component';
import {MODAL_MOBILE_WIDTH, MODAL_WIDTH} from '../components/public/header/header.component';
import {ErrorComponent} from '../components/public/@dialogs/error/error.component';
import {SUCCESS_MODAL_MOBILE, SUCCESS_MODAL_WIDTH} from '../app-constants';
import {LoginComponent} from '../components/public/auth/login/login.component';
import {ApiService} from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loginError;
  registrationError;
  private _regData: any = {};
  avatar;

  private _socialToken = JSON.parse(localStorage.getItem('@gmooh:socialToken')) || {};

  constructor(
    private api: ApiService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<any>,
    private window: WindowService
  ) { }


  set regData(data) {
    this._regData = Object.assign({ ...this._regData }, data);
  }

  get regData() {
    return this._regData;
  }

  clearRegData() {
    this._regData = {};
  }

  set socialAccountToken(data) {
    this._socialToken = Object.assign({ ...this._socialToken }, data);
    localStorage.setItem('@gmooh:socialToken', JSON.stringify(this._socialToken));
  }

  get socialAccountToken() {
    return this._socialToken;
  }

  clearSocialAccountToken() {
    this._socialToken = {};
    localStorage.removeItem('@gmooh:socialToken');
    // this.clearDataGoogle();
  }

  sendForgotPassRequest(email) {
    this.api.forgotPassword(email).subscribe( res => {
      this.openForgotPasswordModal({
        status: res.status,
        message: res.errorMessage
      });
    });
  }

  openSuccessModal(): void {
    this.dialog.open(SuccessComponent, {
      width: !this.window.isDesktop ? MODAL_MOBILE_WIDTH : MODAL_WIDTH,
      data: {
        text: 'Congratulations, you can plan, save and share your plans with Gmooh now!'
      }
    });
  }

  openForgotPasswordModal(config) {
    const component = config.status === 'SUCCESS' ? SuccessComponent : ErrorComponent;
    this.dialog.open(component, {
      width: !this.window.isDesktop ?  SUCCESS_MODAL_MOBILE : SUCCESS_MODAL_WIDTH,
      data: {
        text: config.message || 'Check your email',
        status: config.status
      }
    });
  }

  openMoreInfoModal(): void {
    this.dialog.open(ProfileMoreInfoComponent, {
      width: '530px',
      data: {
        isMobile: !this.window.isDesktop
      }
    });
  }

  openLoginModal(): void {
    this.dialog.open(LoginComponent, {
      width: !this.window.isDesktop ? MODAL_MOBILE_WIDTH : MODAL_WIDTH,
      data: {
        isMobile: !this.window.isDesktop,
      }
    });
  }
}
