import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { RouteService } from './route.service';
import { Router } from '@angular/router';
import { IPlaceInfo } from '../interfaces/places';
import { BookingDetailsService } from './booking/booking-details.service';
import {SearchService} from './search.service';

@Injectable({
  providedIn: 'root'
})

export class EventPageService {
  constructor(
    private routeService: RouteService,
    private router: Router,
    private apiService: ApiService,
    private search: SearchService,
  ) {}

  goToEvent(id: string, data: IPlaceInfo[], section: string, cityId: number): void {

    if (this.routeService.isCityPage) {
      this.apiService.userClickedData = { isEventType: section };
      this.apiService.userClickedData = { chosenCityItem: data };
    }

    this.search.flightsData = { id, placeId: id };

    const isEvent = section.includes('Event') ? 1 : 0;
    const queryParams = {
      placeId: id,
      isEvent: isEvent,
    };
    if (!isEvent) {
      queryParams['cityId'] = cityId;
    }
    this.router.navigate([ 'event' ], { queryParams: queryParams});

  }
}
