const express = require('express');
const routes = require('./routes');
const path = require('path');
const fs = require('fs');
const https = require('https');
const http = require('http');

module.exports = function ({ port, type }) {

  const app = express();

  app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', true);
    next();
  });

  routes(app);

  app.set('port', port);
  app.use('/', express.static(appRoot + '/dist/gmooh/'));
  app.get('/*', (req, res) => res.sendFile(path.resolve(appRoot + '/dist/gmooh/index.html')));

  if (type === 'http') {
    http.createServer(app).listen(port, () => {
      console.log(`API running on localhost:${port}`);
    });
  } else {
    const privateKey  = fs.readFileSync('/home/ubuntu/cert/gmooh.app.key', 'utf8');
    const certificate = fs.readFileSync('/home/ubuntu/cert/gmooh.app.chained.crt', 'utf8');
    const credentials = {key: privateKey, cert: certificate};

    https.createServer(credentials, app).listen(port, () => {
      console.log(`API running on localhost:${port}`);
    });

  }


};

