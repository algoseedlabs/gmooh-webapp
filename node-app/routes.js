const jsonParser = require("body-parser").json();
const readWriteAsync = require('./api/edit-meta');

module.exports = function (app) {
  app.post('/edit-meta', jsonParser,  (req, res) => {
    readWriteAsync(req.body, res);
  });
};
