const fs = require('fs');


module.exports = function ({ image, title, url }, res) {
  const _path = appRoot + '/dist/gmooh/index.html';

  fs.readFile(_path, 'utf-8', (err, data) => {
    if (err) throw err;
    const searchValue = /<meta property="og:image" content=".*">/g;
    const replaceValue = `<meta property="og:image" content="${image}">`;
    const searchValue_2 = /<meta property="twitter:image" content=".*">/g;
    const replaceValue_2 = `<meta property="twitter:image" content="${image}">`;
    const searchValue_3 = /<meta property="og:title" content=".*">/g;
    const replaceValue_3 = `<meta property="og:title" content="${title}">`;
    const searchValue_4 = /<meta property="twitter:title" content=".*">/g;
    const replaceValue_4 = `<meta property="twitter:title" content="${title}">`;
    const searchValue_5 = /<meta property="og:url" content=".*">/g;
    const replaceValue_5 = `<meta property="og:url" content="${url}">`;
    const searchValue_6 = /<meta property="twitter:url" content=".*">/g;
    const replaceValue_6 = `<meta property="twitter:url" content="${url}">`;

    const newValue = data
      .replace(searchValue, replaceValue)
      .replace(searchValue_2, replaceValue_2)
      .replace(searchValue_3, replaceValue_3)
      .replace(searchValue_4, replaceValue_4)
      .replace(searchValue_5, replaceValue_5)
      .replace(searchValue_6, replaceValue_6);

    fs.writeFile(_path, newValue, 'utf-8', (err) => {
      if (err) {
        res.sendStatus(500);
      }
      res.sendStatus(200);
    });

  });
};
